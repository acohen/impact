You are invited to submit a short paper (2 pages excluding 
references) to the IMPACT'22 workshop (12th International 
Workshop on Polyhedral Compilation Techniques), held in 
conjunction with HiPEAC 2022 in Budapest, Hungary.

Short papers are a good venue for projects that are still
work-in-progress, tool demonstrations, etc. Submissions should
not exceed 2 pages, excluding references. The deadline for full
paper submissions has already passed.

The workshop will be held in hybrid mode (both in person and
online).

Website: https://acohen.gitlabpages.inria.fr/impact/impact2022/

======================================
IMPORTANT DATES:
Full paper submission:  Closed (November  7, 2021).
Short paper submission: December 15, 2021 (AoE)
Author notification:	December 22, 2021
Final version due:	January   7, 2022 (AoE)
Workshop:		January  17, 2022
======================================

Details about the short paper format and submission guidelines
are available on the website of the workshop:
    https://acohen.gitlabpages.inria.fr/impact/impact2022/

--------------------------------------------------------------
*** 
*** SUBMISSION NUMBER: 11
*** TITLE: On Models for Data Parallel Streaming Applications
*** AUTHORS: Rosilde Corvino, Cedric Nugteren, Bart Kienhuis, Lech Jozwiak, Henk Corporaal
*** CATEGORY: regular paper
*** 
*** SEND TO: impact-chairs@lists.gforge.inria.fr
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:

This paper deals with parallel data streaming applications. the authors are focusing on a efficient model to analyse and optimize the different steps of the design of multi-threading systems for data parallel streaming.They start by giving the characteristics of three main models: Polyhedral model , CSDF or PPN Model and ARRAY-OL model . A comparison between these models is dressed. 
The authors deduce that since each model has its specificities and provides optimisation of specific steps of the data streaming design, it will be important to use both of them and to switch from a model to another during the analysis. A conversion methods from PPN to Array-OL and from Array-OL to PPN models was proposed and explained through an example. 


--- Strengths:

the conversion between the two models.

--- Weaknesses:

The benefits of the combination between the two models need to be explained with more details and illustrated with an example.

--- Detailed comments:

Page 2 : section 4 paragraph after Figure 2, a reference is missing: "In[], Glitia et al..."

Section 6.2 

Listing 2 : Triangularization Algorithm 
 
the condition of the while , I think that it is rather

"while ( (ord_temp<>rod) and (BestW<>0) )" instead of "while ( (ord_temp<>rod) and (BestW==0) )", since if (BestW==0), you don't need to iterate, the matrix is already triangulated.

Listing 3. the initialization of indices i, j and variable found are missing.

Notations G(i) and G(j) are not introduced, even if we can understand after some investigations that it is related to tasks.




--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

For me, this paper is not providing anything "new".

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

---   3 (strong accept)
---   2 (accept)
---   1 (weak accept)
---  -1 (weak reject)
---  -2 (reject)

1

*** REVIEWER'S CONFIDENCE:

---  4 (expert)
---  3 (high)
---  2 (medium)
---  1 (low)
---  0 (null)

2

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)
---  2 (medium)
---  1 (mild)

3

*** TECHNICAL SOUNDNESS

---  4 (bullet proof)
---  3 (minor problems)
---  2 (ok, but some problems need to be addressed)
---  1 flawed

3

*** END
--------------------------------------------------------------

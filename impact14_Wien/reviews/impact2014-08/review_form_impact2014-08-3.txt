--------------------------------------------------------------
*** 
*** SUBMISSION NUMBER: 08
*** TITLE: On the Variety of Static Control Parts in Real-World Programs: from
*** AUTHORS: Andreas Simb�rger, Armin Gr��linger
*** CATEGORY: regular paper
*** 
*** SEND TO: impact-chairs@lists.gforge.inria.fr
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:

The paper presents an experimental analysis of the proportion
of the dynamic execution time of real world programs that
is amenable to polyhedral analysis and extensions thereof.

--- Strengths:

- detailed experimental results
- delinearization algorithm

--- Weaknesses:

- misses important related work
- somewhat tailored to a non-polyhedral issue in a particular compiler

--- Detailed comments:

There is no comparison to SPolly, which seems very much related
and which should also have been known to the authors.

The delinearization algorithm should be compared to that of
Maslov and Pugh's "Simplifying Polynomial Constraints Over Integers
to Make Dependence Analysis More Precise".  The authors only
compare against an earlier paper by Maslov that is superseded
by this work.

Since this paper appears to be mainly focused on LLVM/Polly and
whether/how it should be extended, it may be more suitable for
an LLVM related venue.

What does "not parametric coefficient" mean in

    into several equations. This algorithm works on the coeffi-
    cients (not parametric coefficient) of the equation and uses
    integer operations such as greatest common divisor; there-

?

The Ptr class is not entirely clear.  Did you implement the changes
you describe in 3.2?  If so, then doesn't this class become part
of Static?

    For our study, we also
    determined an optimistic number for the pointer to pointer
    arrays by disabling Polly's check for aliasing.

Did you report these optimistic numbers?
They don't seem be mentioned in Table 1.

Formula (1) and the following formulas would be easier to understand
if you made the dependence on parameters and iterators more explicit,
i.e., \gamma_x(\vec i, \vec i').

    Note that Spp , Sppa , Smd and Sna give the number of ad-
    ditional Scops compared to Static . For class Dynamic , the
    number given is the total number of Scops in this class.

Why this difference?  Also, this description doesn't seem
to match the data in Table 1 as there are several benchmarks
where the S_jit number is smaller that then S_st number.
Minor nit: since the subscripts are not variables, they you be typeset in roman.

    No. of affine dependences in class Static.

Is this all the affine dependences or only those that are not uniform?

Typos:

p1:
- "As an example for" -> "As an example of"
- "delivered with the these systems" -> "delivered with these systems"
- "the aim to achieve" -> "the aim of achieving"
- "working ... bring new challenges" -> "working ... brings new challenges"
p3:
- "the linear algebra" -> "linear algebra"
- "is is not possible" -> "is not possible"
p4:
- "When wee say" -> "When we say"
p5:
- "neglects the integrality of the variables" -> "ignores ..."
p6:
- "with program package" -> "with the program package"
- "found in in class Static" -> "found in class Static"

--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

---   3 (strong accept)
2 (accept)
---   1 (weak accept)
---  -1 (weak reject)
---  -2 (reject)

*** REVIEWER'S CONFIDENCE:

---  4 (expert)
---  3 (high)
2 (medium)
---  1 (low)
---  0 (null)

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)
2 (medium)
---  1 (mild)

*** TECHNICAL SOUNDNESS

4 (bullet proof)
---  3 (minor problems)
---  2 (ok, but some problems need to be addressed)
---  1 flawed


*** END
--------------------------------------------------------------

#!/usr/bin/perl

# Populate a mailing list with its members and set some defaults.
#
#	setup_list.pl name_of_list
#
# name_of_list is, e.g., "2014-01".
# The list is assumed to have been created using create_list.pl
# and the password of the list is assumed to have been saved in
# the "list_pw" file.  That is, this file should contain a line
# of the form
#
#	2014-01 password
#
# where "password" is the password of the list.
# The file "impact$list/list", i.e., "impact2014-01/list" in the example
# is assumed to contain the members of the list, one per line.
# Unfortunately, any characters other that ASCII characters are
# currently not supported.

use WWW::Mechanize;
use Encode qw(from_to encode is_utf8 _utf8_on);
use utf8;

my $list = $ARGV[0];
my $pw_file = "list_pw";

my $list_pw;

open my $fh, "<", $pw_file;
while (<$fh>) {
	my ($id, $pw) = split;
	$list_pw = $pw if $id eq $list;
}
close $fh;

my $members;

{
	local $/;
	my $filename = "impact$list/list";
	open my $fh, "<", "$filename" or die "cannot open file $filename";
	$members = <$fh>;
	close $fh;
}

print "$list $list_pw\n";
print "$members\n";

my $mech = WWW::Mechanize->new();
my $form;
my $reponse;
my $request;

$mech->get("https://lists.gforge.inria.fr/mailman/admin/impact-$list");
print $mech->status(), "\n";
print $mech->title(), "\n";
$form = $mech->form_number(1);
$mech->set_fields((adminpw => $list_pw));
$response = $mech->click;
#print $response->decoded_content;
print $mech->status(), "\n";
print $mech->title(), "\n";
$mech->follow_link(url_regex => qr/general/);
$form = $mech->form_number(1);
$mech->field('reply_goes_to_list', 1);
$mech->untick('new_member_options', 'nodupes');
$response = $mech->click;
$mech->follow_link(url_regex => qr/privacy/);
$mech->follow_link(url_regex => qr(privacy/sender));
$form = $mech->form_number(1);
$mech->field('generic_nonmember_action', 0);
$response = $mech->click;
$mech->follow_link(url_regex => qr/archive/);
$form = $mech->form_number(1);
$mech->field('archive', 0);
$response = $mech->click;
$mech->follow_link(url_regex => qr/language/);
$form = $mech->form_number(1);
$mech->tick('available_languages', 'en');
$response = $mech->click;
$form = $mech->form_number(1);
$mech->field('preferred_language', 'en');
$response = $mech->click;
$mech->follow_link(url_regex => qr/members/);
$mech->follow_link(url_regex => qr(members/add));
$form = $mech->form_number(1);
#$form->accept_charset("iso-8859-1");
_utf8_on($members);
#from_to($members, "utf8", "iso-8859-1");
$mech->field('subscribees', $members);
#$form->dump;
#$request = $form->click;
#my $ct = $request->header("Content-Type");
#$ct .= "; charset=UTF-8";
#$request->header("Content-Type", $ct);
#print $request->as_string;
#$response = $mech->request($request);
#print $response->decoded_content;
$response = $mech->click();

--------------------------------------------------------------
*** 
*** SUBMISSION NUMBER: 10
*** TITLE: Parametric Tiling with Inter-Tile Data Reuse 
*** AUTHORS: Alain Darte, Alexandre Isoard
*** CATEGORY: regular paper
*** 
*** SEND TO: impact-chairs@lists.gforge.inria.fr
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:
The paper deals with the question of modelling data reuse from
one tile to the next (tiles are executed sequentially on an
accelerator) in tiled coded when the tiling is parametric.
The main insight of the paper is that to model the elements
accessed by preceding (or succeding) tiles it is not necessary
to deal with non-linear expressions.

--- Strengths:
The paper contributes to the important problem of parametric tiling
and provides an interesting insight.

--- Weaknesses:
The paper is very dense and is not exactly self-contained (relies a
bit much on the reader to be familiar with cited previous
work). Section 3.2 is rather hard to follow (this reviewer couldn't
completely).

--- Detailed comments:

After introducing the problem, the authors give a good explanation of
their aim, namely inter-tile data reuse. The paper assumes that tiles
are executed sequentially on an accelerator. Since accesses to main
memory are assumed to be expensive, the question is which elements to
keep in the local memory of the accelerator, i.e., when to load data
into the local memory and when to write in back to main memory. To
compute the sets of elements to load/remove from local memory for each
tile, polyhedral operations can be used. When parametric tiling is
applied, the challenge is that the description of the tiles include
non-linear expressions. The paper shows that the computations can be
performed without relying on these non-linear expressions. The main
observation is that the non-linearities arise from the condition that
tiles are translated by an integer multiple of the lattice vectors
describing the tiling. For the application shown in the paper, one can
equivalently deal with all "unaligned" tiles that have arbitrary
offsets wrt. a given tile (not only integer multiples of the lattice
vectors). For example, taking the union of the elements accessed by all
tiles preceding a given tile then does not require any non-linear
conditions and yields the same result.

The explanations given in the paper are succinct but not too difficult
to follow up to (and including) Section 3.1. Section 3.2 presents an
extension of the technique to the case where the sets of elements on
which the computations are based (In, Out, etc.) are approximations of
the actual sets. This, of course, makes correct execution of the
program more difficult (e.g., an overapproximated Out set should not
cause an up-to-date element in the main memory to be overwritten
erroneously by a (random) value from local memory at the end of a
tile). Unfortunately, the presentation in Section 3.2 is very dense
and this reviewer cannot follow the derivations in detail. The section
start by "importing" definitions from two other papers (references [3]
and [2]) and after that further equations and set names (e.g., "Ra")
are introduced on the fly without much explanation.

Section 3.2 is simply too dense to be easy to follow. In this
reviewer's opinion, Section 3.2 should be removed or rewritten
completely. The authors say that they are not giving any proofs;
therefore, the derivations can also be removed. The reader would be
helped more by an explanation of the result (the formulas the authors
aim for), e.g., Property 4, and proofs/derivations can be put in an
appendix or a technical report.

Section 3.1 also starts with an "import" (from reference [8]). Here,
the dependence on another paper could be reduced by a few sentences
about what is actually required in the present paper.

Section 4 presents the results computed by the presented technique for
several codes from PolyBench. For several codes, the results are not
shown because they have "too many instructions" or "too many
variables". This makes the selection of presented results a bit
biased. Maybe the authors could put all the results on a web page (or
a supplement file for the submission) to avoid the bias towards
"small" results in the presentation.

The work in this paper is clearly very relevant for IMPACT and the
polyhedral community. The denseness of the presentation can likely be
ameliorated without too much effort (by focusing on the result in
Section 3.2) for the final version. Therefore, this reviewer rates the
paper "weak accept". (The "technical soundness" has been given as
"minor problems" as this reviewer could not completely follow the
presentation even at the beginning of Section 3.2, see also the
comment below - the paper may actually be "bullet proof" but this
reviewer cannot tell. Up to Section 3.1 this reviewer believes the
derivations are sound.)


Section 3.1, Definition 1: "C\in P(A)" should be "C\subseteq P(A)".

Section 3.2: Shouldn't (7) start with "\overline{Store}"?
Shouldn't the "\cap" in (8) be a "\cup"? What is the role of "Ra"?


--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

---   3 (strong accept)
---   2 (accept)
   1 (weak accept)
---  -1 (weak reject)
---  -2 (reject)

*** REVIEWER'S CONFIDENCE:

---  4 (expert)
  3 (high)
---  2 (medium)
---  1 (low)
---  0 (null)

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)
---  2 (medium)
  1 (mild)

*** TECHNICAL SOUNDNESS

---  4 (bullet proof)
  3 (minor problems)
---  2 (ok, but some problems need to be addressed)
---  1 flawed


*** END
--------------------------------------------------------------

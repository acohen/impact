#!/usr/bin/perl

# Create a new mailing list;
#
#	create_list.pl name_of_list
#
# name_of_list is, e.g., "2014-01".
#
# You'll receive an email from gforge with the list password.
# Assuming the password is "password", add the following line
# to the file "list_pw".
#
#	2014-01 password
#
use WWW::Mechanize;

my $list = $ARGV[0];

print "$list\n";

my $mech = WWW::Mechanize->new();
my $reponse;
my $form;

$mech->get('https://gforge.inria.fr/mail/admin/index.php?group_id=3477&add_list=1');
print $mech->status(), "\n";
print $mech->title(), "\n";
$mech->form_number(2);
$mech->set_fields((form_loginname => 'skimo', form_pw => 'TxcLA26g'));
$response = $mech->click;
print $mech->status(), "\n";
print $mech->title(), "\n";
$form = $mech->form_number(3);
$mech->set_fields((list_name => $list, is_public => 0));
$response = $mech->click;
print $mech->status(), "\n";
print $mech->title(), "\n";

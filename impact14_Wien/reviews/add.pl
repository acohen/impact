#!/usr/bin/perl
my $edition = "impact2014";
my $prefix = "review_form_$edition";
my $mapname = "map";
my $filename = $ARGV[0];

my %map;
open my $map, "<", $mapname;
while (<$map>) {
	my @el = split;
	my $paper = shift @el;
	$paper =~ s/://;
	$map{$paper} = [@el];
}
close $map;

my $form;

{
	local $/;
	open my $fh, "<", $filename or die "cannot open file $filename";
	$form = <$fh>;
	close $fh;
}

my ($id) = $form =~ /REVIEW FORM ID: ([0-9:]+)/;
my ($paper) = $form =~ /SUBMISSION NUMBER: ([0-9]+)/;

my $n = $#{$map{$paper}} + 1;
for (0..$#{$map{$paper}}) {
	if ($map{$paper}[$_] eq $id) {
		$n = $_;
		last;
	}
}
if ($n > $#{$map{$paper}}) {
	push @{$map{$paper}}, $id;
}

my $name = "$edition-$paper";
mkdir $name;

$name .= "/$prefix-$paper-$n.txt";
print "$filename -> $name\n";

$form =~ s/REVIEW FORM ID:.*//;
$form =~ s/PC MEMBER:.*//;
$form =~ s/\*\*\* REVIEWER'S .*: .*\n.*\n//mg;

open my $fh, ">", $name or die "cannot open file $name";
print $fh $form;
close $fh;

open my $map, ">", $mapname;
for my $key (keys %map) {
	print $map "$key: " . (join " ", @{$map{$key}}) . "\n";
}
close $map;

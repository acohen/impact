--------------------------------------------------------------
*** 
*** SUBMISSION NUMBER: 12
*** TITLE: Tiling for Dynamic Scheduling
*** AUTHORS: Ravi Teja Mullapudi, Uday Bondhugula
*** CATEGORY: regular paper
*** 
*** SEND TO: impact-chairs@lists.gforge.inria.fr
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:

    This paper presents an approach for tiling that is less restrictive than approaches that require the tiles to be executable with an affine schedule.  Tile execution is still atomic and no cycles between tiles are allowed, but the dependence graph between tiles can be complex and run-time scheduling of the tile task graph can be performed.  Experimental results of prototyping this technique by hand for a couple of benchmarks shows that performance improvements are possible because tiling becomes possible.  The paper also describes and algorithm for tiling using the less restrictive requirements.


--- Strengths:

    + The techniques presented enable tiling to be applied to a wider range of computations.
    
    + The experimental results are promising.
    
    + Appropriate related work is cited.
    
    + The paper is well organized and reads well.


--- Weaknesses:

    - The techniques prototyped are not fully automated yet.


--- Detailed comments:

    This is a really interesting paper, and it is well written.  The algorithm for performing tilings still needs to have tile splitting incorporated, but it can already do one of their examples.  The experimental results show that when both examples are prototyped there are performance benefits.  The IMPACT community will want to know about this direction of research.
    
    Below are some suggestions for some minor fixes.

Minor Issues
    - "transformation which has been used" -> "transformation that has been used"
    
    - Need a sentence or two at the beginning of Section 2 indicating why the polyhedral model is being explained.  Everyone at IMPACT knows, but you will want other people to read this paper as well.
    
    - "hyperplanes which satisfy the constraints" -> "hyperplanes that satisfy the constraints"
    
    - "The hyperplane which lexicographically minimizes" -> "The hyperplane which lexicographically minimizes"
        Not going to show incorrect usage of "which" anymore.  Should look up the rule online and fix the rest of the paper.
        
    - Having Sections 2.1 and 3.1, but no 2.2 or 3.2 is organizationally awkward.
    
    - For (3), should state that E is the set of edges in the GDG.
    
    - Example 2, check the dependencies and the code.  If the constraints are really supposed to be (i<j-1) and (i>j-1) instead of (i<j) and (i>j), then there are some extra dependence arrows shown in the Figure.  Also, it appears that there are some edges missing.
    
    - "Figure 3 illustrates the contradiction",  It is currently called Example 3.  Also, Example 3 needs a caption.  Oh wait, there is a Figure 3 and an Example 3.  That is confusing.  Please use consistent labeling.
    
    - Figure 3 shows a cycle between the 1D green tiles, but not amongst the 2D tiles.  The figure doesn't appear to be illustrating what is indicated in the caption.
    
    - At the beginning of Section 4.2 tell us how you plan to use the definitions.
    
    - "This new approach finds the desired tiling for Example 1." but not Example 2.
    



--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

   3 (strong accept)
---   2 (accept)
---   1 (weak accept)
---  -1 (weak reject)
---  -2 (reject)

*** REVIEWER'S CONFIDENCE:

  4 (expert)
---  3 (high)
---  2 (medium)
---  1 (low)
---  0 (null)

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)
  2 (medium)
---  1 (mild)

*** TECHNICAL SOUNDNESS

  4 (bullet proof)
---  3 (minor problems)
---  2 (ok, but some problems need to be addressed)
---  1 flawed


*** END
--------------------------------------------------------------

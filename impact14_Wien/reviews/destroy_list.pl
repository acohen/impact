#!/usr/bin/perl

# Remove a mailing list created by create_list.pl;
#
#	destroy_list.pl name_of_list
#
# name_of_list is, e.g., "2014-01".

use WWW::Mechanize;
use Data::Dumper;

my $list = $ARGV[0];

print "$list\n";

my $mech = WWW::Mechanize->new();
my $reponse;
my $form;
my $content;
my $group_list_id;

$mech->get('https://gforge.inria.fr/mail/admin/?group_id=3477');
print $mech->status(), "\n";
print $mech->title(), "\n";
$mech->form_number(2);
$mech->set_fields((form_loginname => 'skimo', form_pw => 'TxcLA26g'));
$response = $mech->click;
print $mech->status(), "\n";
print $mech->title(), "\n";
$content = $response->decoded_content;
($group_list_id) = $content =~ qr/impact-$list<.*?group_list_id=(\d+)/;
print $group_list_id, "\n";
$mech->get("https://gforge.inria.fr/mail/admin/deletelist.php?group_id=3477&group_list_id=$group_list_id");
print $mech->status(), "\n";
print $mech->title(), "\n";
$form = $mech->form_number(3);
$mech->tick('sure', '1');
$response = $mech->click;
print $mech->status(), "\n";
print $mech->title(), "\n";

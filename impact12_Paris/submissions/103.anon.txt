Review for paper #103

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept, 5:strong accept)
5

---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3: knowledgeable, 4: expert)
4

---
Potential for Live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)
3

---
Paper Summary 
(<= 10 lines)

This paper deals with precisely generating sets of data to be moved in 
and out of host memory when computation has been mapped on to 
accelerators. This problem is of high interest.  It is applicable to 
systems with accelerators and, in general, to compile for heterogeneous 
systems. There is currently no or limited compiler support in this 
space, and techniques like these are very welcome.

---
Paper Strengths 
(list of items)

1. A comprehensive and significant solution to a very important problem

2. An elegant formalism and a good presentation of it.

3. Approach is powerful - since it works given an arbitrary schedule in 
   the polyhedral representation

---
Paper Weaknesses 
(list of items)

---
Other Comments (disclosed to authors)

It has been known well that a solution like this would be possible with 
available polyhedral machinery and techniques, but, as far as I know, 
nothing has been published on this topic. Existing approaches on 
automatic data movement (Baskaran et al. PPoPP 2008 for eg) are very 
naive in comparison with this work (both solve different problems, but 
of same flavor). Techniques presented in this paper can be generalized / 
recycled to address a number of similar data movement problems.

Formulae in 3.1 (Theorem 1) are defined quite elegantly. The next step 
is to find a good way to compute them - which is a separate step - 
Section 3.2.  I find the presentation nice. 

Have you considered an alternate dependence-based definition for your 
Load(T) and Store(T) as follows:

Load(T) = {\vec{m} in In(T) such that no RAW dep edges reach m}
Store(T) = {m in Out(T) which have no WAW edges originate from m} 

Once you compute your deps with PIP (with last writer), you can express 
the above with polyhedral set operations alone (no need of any lex min, 
max). Also, with the above approach, In(T) and Out(T) can just be all 
the reads and writes in T resp. You'll have to repeatedly subtract from 
In(T), Out(T), points obtained from relevant dep edges.

The approach taken by the authors is to define and construct these sets 
without relying on dependences, and hence, use PIP heavily for this 
purpose -- to obtain Load(T), Store(T). This is an interesting approach.  
Clearly, there are many ways to compute these sets once they are clearly 
defined.

Read(m,S) = {.... u(i) = i_0 }

i_0? It should have been \vec{m}

What's b?  I don't see why you need to do the floor stuff and compute 
Read(m,S) that way. How about computing it by just regenerating D and u 
based on the schedule, i.e., generate the new domain and access 
function, and use the original definition.

Last para of 3.2 should be written more rigorously.

Mention clearly that scheduling functions (\theta) permeate all of the 
sets: In, Out, etc., i.e., construction of all of these sets is for a 
given schedule. If one specifies a different transformation, the scheme 
should/will work transparently and seamlessly.

Last para in Section 2 about libraries: you don't need to mention about 
specific libraries right here. Describe your solution to construct these 
sets and then say how various libraries are used. 

Inter-tile reuse
Though you say you exploit inter-tile reuse, at that point, it's not 
clear if you exploit all inter-tile reuse. Inter-tile reuse may exist in 
multiple directions and exploiting them in more than one direction will 
require buffer sizes that are a function of problem size.

Finally, to conclude, I feel a more efficient formulation for computing 
these sets may be possible. This discussion can be left for the 
workshop.


Minor comments 

What if a value is written to at the accelerator, but is not finally 
needed by the host. You need to have a one final subtraction for data 
that is not live out from the accelerator.

Hoist the definition of T_max out of the parenthesis and, better, out of 
the theorem. 

amounts to hoist -> amounts to hoisting

The 3 conditions -> The three conditions

Section 3.2  First, say what is \vec{m}.

---------------------
**********************************************************************

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept,
5:strong accept)
3: weak accept

---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3: knowledgeable,
4: expert)
4: expert

---
Potential for Live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)
3: hot

---
Paper Summary
(<= 10 lines)
This paper describes an automatic method to generate memory transfers for
the purpose of offloading kernels to FPGA.
PIP is used to compute the exact set of loads and stores, mechanisms are
described for approximate load stores.
Another contribution of this paper is a mechanism to generate source-level
code to synchronize communications and computations.
This mechanism uses software pipelined communicating processes to perform
these synchronizations.
Both techniques are more generally applicable than FPGAs.
The paper relies an external tool or user to find schedules and tilings and
can generate optimized transfers for seemingly any tiling specification.
To optimize reuse across transfers, the contribution utilizes the following
execution limitations:
- the first loop encompassing tiles is executed sequentially on the
accelerator, reuse is exploited across tiles using this loop
- the size of memory locations offloaded on the accelerator must fit in the
accelerator memory for the whole duration of a tile strip (this does not
seem stated explicitly and has implications on admissible tile sizes)
Local memory management uses the superior technique of modular mappings.
The experimental section looks at simple kernels and gives meaningful
quantitative evidence of the interest of the technique.

---
Paper Strengths
(list of items)
- very well written paper
- very clear and precise formalization of the computation of optimized
transfer sets
- technique is generally applicable *given the limitations* stated in the
Paper Summary
- paper shows an elegant solution to the problem of generating
multi-buffered communications

---
Paper Weaknesses
(list of items)
- forcing innermost inter-tile dimension sequentiality may completely
destroy parallelism (e.g. 2-D wavefront parallel loops)
- no discussion about memory liveness implications on tile sizes: a band of
tiles must fit in memory and even with modular mappings this has
implications on tile sizes and therefore on reuse
- optimization of communications is limited to the case where a memory
location A is read/written to a local memory location B and *the same*
memory location A is later read again in *the same* memory location B. Many
(most ?) important reuse patterns do not fit this description (for instance
a simple 2-D jacobi kernel)
- failure to credit previous existing work

---
Other Comments (disclosed to authors)
In the introduction, the section "however integrating ... reserved to the
expert designer" is wrong.
The paper "Productivity via Automatic Code Generation for PGAS Platforms
with the R-Stream Compiler" by Meister et al. describes a compiler flow
that handles all these issues with support for multi-buffering. In
particular the figure 5 of that paper shows automatically produced
multi-buffered code with redundant communication removed.
This missing reference is a problem and is the reason of the "weak accept"
grade to an otherwise very good paper.
This reviewer will gladly bump up the grade to "accept" if the authors
commit to citing this reference properly.

In section 3, "Usually the approach ... forbids to overlap communications
and computations" seems incorrect.
The claim is very dependent on the definition and the size of a tile, a
tile strip and the way communications are generated.
For instance, if your local store has size M and tiling enforce a tile has
footprint M/N then it seems possible to do N-way multi-buffering, across
tiles, in the presence of dependences.
To this effect, reference [20] which shows an automatic mapping path for
GPUs with the RStream compiler is not relevant to these author's work.
The reference "Productivity via Automatic Code Generation for PGAS
Platforms with the R-Stream Compiler" is the one that must be cited.

A section on the current limitations of the technique would be useful to
clarify the current weaknesses and general ideas to attack them in the
future.

---
**********************************************************************

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept,
5:strong accept)
5
---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3:
knowledgeable, 4: expert)
4
---
Potential for Live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)
2
---
Paper Summary
(<= 10 lines)
This paper decribes techniques for optimizing remote accesses for
an accelerator (in this case an FPGA) that communicates with an external
memory (in this case a DDR). They describe how to efficiently generate
sets of accessed data so that reuse is exploited and communication is 
reduced; in addition, they present a code generation scheme. 
---
Paper Strengths
(list of items)
+ This paper advances the state-of-the-art in fully automating high-level
synthesis of FPGA kernels.
---
Paper Weaknesses
(list of items)
- While the authors believe that their work is of possible interest
to other accelerators (e.g., GPUs), it would have been useful to see
some demonstration of that. 
---
Other Comments (disclosed to authors)
+ This is a solid paper worth accepting.
---
**********************************************************************

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept, 5:strong accept)
4

---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3: knowledgeable, 4: expert)
3

---
Potential for live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)
2

---
Paper Summary 
(<= 10 lines)

The paper proposes a complete approach to the optimization of memory
transfers between DDR memory and a kernel running on a hardware
accelerator or processor with local memory. It takes the standpoint,
common in designs for FPGAs, of controlling the memory operations
explicitely, and specifically for a given kernel. A combination of
locality-enhancing (tiling, detection of inter-kernel reuse), latency
hiding (pipelining), bandwidth optimization (coalescing), and
fine-grain instrumentation of the kernel with memory operations. 3
kernels are evaluated on an FPGA, comparing the results with
high-level compilers.

---
Paper Strengths 
(list of items)

Hard problem, addressed thoroughly.

Original method to approximate communicated sets and inter-kernel reuse.

---
Paper Weaknesses 
(list of items)

The need to operate the memory transfers in a kernel-specific way is
not established. A comparison with alternative approaches is needed,
relying on block transfers and local load/store decoupling buffers.

Many details with PIP and Polylib are unnecessary. A higher
level algorithmic description would help readability.

---
Other Comments (disclosed to authors)

The paper misses a well-identified treatment of related work. Although
it is not strictily mandatory to have a dedicated section, the reader
is left wondering about novelty.

There are a huge number of techniques combined into this paper. This
is both a strength and a weakness because of the lack of evaluation of
each one of them individually. For example, the so-called
linearization technique based on Boulet and Feautrier's algorithm is
not compared with alternatives.

---
**********************************************************************

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept,
5:strong accept)
4
---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3:
knowledgeable, 4: expert)
3
---
Potential for Live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)
2
---
Paper Summary
(<= 10 lines)
This paper presents a technique for transferring data to and from an FPGA.
The input code is first tiled and data communication is optimized with
respect to the innermost tile loop.  In particular, data is loaded right
before the first undefined read in this innermost tile loop and stored
right after the last write.  Furthermore, the communication is pipelined
with respect to the computation and the local memory storage is optimized
using lattice-based memory allocation.  Experiments on some small kernels
show that the automatic approach achieves nearly the same speed-ups
as manual optimization.
---
Paper Strengths
(list of items)
- simple and elegant technique
---
Paper Weaknesses
(list of items)
- drowned by low-level implementation details
- little to no comparison to related work
---
Other Comments (disclosed to authors)

My main problem with this paper is that it spends an inordinate amount
of space on explaining low-level manipulations of "quasts".
Since the authors themselves do not appear to consider this as a contribution
of the paper (it's not mentioned in the abstract or the list of contributions),
most if not all of these low-level explanations should be removed.
Instead, the authors could include more details from their technical
report on approximated reads and writes, which is much more interesting.

It's not clear why the authors are even using quasts.
Clearly quasts have their uses, but it is not clear at all that this
application is one of them.  It appears that the authors are mainly
performing operations on sets, which can be performed much more
conveniently using libraries such as Omega or isl.
For example, the "inversion principle", which the authors claim
has never been exploited so far, appears to be no more than
the inversion of a relation, which is a very basic operation
in both Omega and isl.
As to the remark
    "it is not always clear with Omega when its output is ap-
    proximated, in particular when operations such as set inter-
    sections or differences are used"
unless the authors can somehow substantiate these claims,
this is pure FUD.  The (exact) set difference operation is
explained in section 11.1.2 of Wonnacott's thesis.  Perhaps
the authors have found a bug in Omega, but then they should
report it.  They had better use the latest version of Omega
though (http://chunchen.info/omega/) instead of the outdated
version they mention in the paper.  (It may look new, but
it hasn't seen anything but compilation fixes in about a decade.)
Now, perhaps there are some advantages to using quasts
(perhaps it is faster or perhaps it results in simpler expressions),
but the authors don't seem to mention any.

As to related work, the authors only compare their memory management
to that of [18], but not their algorithm for organizing communication.
They should also compare their work to that of Amini et al.,
"Static Compilation Analysis for Host-Accelerator Communication Optimization".

Figure 1 deserves some more explanations, either in the figure itself
or in the caption.  At the very least, the authors should indicate
the directions of the inner and outer loops after scheduling.
Without this information, the figure is very hard to understand.

The way loop tilings are introduced is very confusing:
    "A loop tiling for a statement S, surrounded by n nested
    loops with iteration domain DS , can be expressed with an
    n-dimensional affine function i \to \theta(S, i),"
This sounds as if \theta represents the tiling, whereas apparently
\theta is a preliminary schedule that is applied before the tiling.
I had to read this paragraph and the following ones several times
before being able to figure this out.


More detailed comments

"if i and j are two vector solutions and is the lexicographic"
-> where

"amounts to hoist transfers out of a tile and regroup the same"
-> hoisting and regrouping

The displayed formula in "Back to the main example" in Section 3.1:
Presumably i, j, ii and jj have been existentially quantified.
This should be made explicit.  Otherwise, there is no way to tell
the difference between those variables and N, which is presumably
a parameter.

"with no need to compute differences of sets"
This is not really true.  The authors have replaced the
In(T) \ In(t < T) by a lexicographic optimization in the computation
of Load (and similarly for Out in the computation of Store),
but the difference with Out(t < T) in the computation of Load is still
being performed.  The fact that they have implemented it with some
trick on the quasts does not mean that they are not performing
the set difference.

"to inverse"
-> to invert

"As this double-buffering
scheme operates on tiles, two by two, this schedule cannot be
specified directly as an affine function."
Perhaps the authors should explain what they mean by "an affine function".
The schedule they end up using, e.g.,

	(Comp, 2p) -> (p,1)

certainly looks affine and it is also a (partial) function.

"mas Then, a C source code, which implements a double-"
missing period

"will not speed up them anyway."
-> speed them up
---
**********************************************************************

---
Overall Merit (1: reject, 2: weak reject, 3: weak accept, 4: accept,
5:strong accept)

5

---
Reviewer Expertise (1: no familiarity, 2: some knowledge, 3:
knowledgeable, 4: expert)

3

---
Potential for Live Questions/Feedback/Fights (1: mild, 2: medium, 3: hot)

1

---
Paper Summary
(<= 10 lines)

This is a paper describing polyhedral compiler analysis to optimize
memory access in VHDL VLSI synthesis.

---
Paper Strengths
(list of items)

The two main contributions of the paper are: 1) improvements to the current
state-of-the-art in computing the load and store operations for a tile,
including the handling of inter-tile reuse; and 2) code generation by
source-to-source transformation to enable pipelined execution with optimized
access to DDR memory, including buffer optimization and management.

---
Paper Weaknesses
(list of items)

The experimental evaluation is weak. Only three small kernels are considered:
1) DMA transfer, 2) Vector sum, and 3) Matrix multiply

---
Other Comments (disclosed to authors)


---
**********************************************************************


for f in 1??-?.txt
do
  awk 'BEGIN { pr=0 }
       { if( $1 ~ /Reviewer:/) { pr=1; next };
         if( $0 ~ /Additional Comments/ ) exit;
         if(pr==1) print; }
  ' $f >$f.anon;
done


for i in 101 102 103 104 105 106 107 108 109 110 111 112 113
do
	echo "Review for paper #$i" >$i.anon.txt
	for f in $i-?.txt.anon
	do
		cat $f >>$i.anon.txt
		echo "**********************************************************************" >>$i.anon.txt
	done
done

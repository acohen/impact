IMPACT 2012 Notification

Dear ,

We are pleased to inform you that your paper 
 ""
has been accepted for oral presentation and publication in IMPACT 2012.

Despite the brief time to the final submission, please consider the
comments of the reviewers carefully for preparing the final version.

Please note that the early registration deadline to HiPEAC is
December 23, 2011: http://www.hipeac.net/conference/paris/registration

The final version of your document must be sent to
   impact-chairs@lists.gforge.inria.fr
by * December 29 * (strict deadline)! The proceeding of all HiPEAC
papers, including workshops, will be distributed on a USB stick to all
participants. Please send us a pdf file by this deadline to have your
paper published.

Please use the style file acm_proc_article-sp.cls available from:
 http://www.acm.org/sigs/publications/acm_proc_article-sp.cls
The document class should be:
\documentclass[preprint]{acm_proc_article-sp}
At the beginning of your document (before the \maketitle command),
insert the following block:
\toappear{
  \hrule \vspace{5pt}
  IMPACT 2012\\
  Second International Workshop on Polyhedral Compilation Techniques\\
  Jan 23, 2012, Paris, France\\
  In conjunction with HiPEAC 2012.\\[10pt]
  http://impact.gforge.inria.fr/impact2012\\
  }

The reviews of your paper and an example tex file respecting the ACM
style are attached to this mail.

We look forward to seeing you at the workshop.

Best regards,
-- 
Uday and Vincent
IMPACT Program Chairs

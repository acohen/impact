#!/bin/sh
## notifier.sh for  in /Users/pouchet
##
## Made by Louis-Noel Pouchet
## Contact: <pouchet@cs.ucla.edu>
##
## Started on  Fri Nov 30 13:44:58 2012 Louis-Noel Pouchet
## Last update Fri Nov 30 16:08:52 2012 Louis-Noel Pouchet
##

DECISION_FILE="decisions_2013.txt";
EMAIL_TEMPLATE_DIR="template_emails";

ACCEPT_template="$EMAIL_TEMPLATE_DIR/accept.txt";
CONDACCEPT_template="$EMAIL_TEMPLATE_DIR/conditional.txt";
REJECT_template="$EMAIL_TEMPLATE_DIR/reject.txt";

PAPER_NOTIFICATION_DIR_TEMPLATE="notif_impact2013_paper";
REVIEW_DIR="../reviews/all_reviews";
REVIEW_FORM_TEMPLATE="review_form_impact2013_";


## Score extractor. Damned those users who cannot follow guidelines!
extract_grade()
{
    start_str="$1";
    end_str="$2";
    num_lines="$3";
    file="$4";

    GRADE="";
    overall=`grep -A $num_lines "$start_str" "$file"`
    has_removed=`echo "$overall" | grep -n "$end_str"`
    if ! [ -z "$has_removed" ]; then
	linenum=`echo "$has_removed" | cut -d : -f 1`;
	linenum=$(($linenum-1));
	piece=`echo "$overall" | head -n "$linenum" | tail -n +2`;

	grade=`echo "$piece" | grep "[0-9\-]" | sed -e 's/---//g' | sed -e 's/ //g' | sed -e 's/\([0-9\-]*\).*/\1/g'`;
    else
	grade=`echo "$overall" | tail -n +3 | grep -v "\-\-\-" | sed -e 's/ //g' | sed -e 's/\([0-9\-]*\).*/\1/g'`;
    fi;
    GRADE="$grade";
    is_a_grade=`echo "$GRADE" | grep "[0-9\-]"`;
    if [ -z "$GRADE" ] || [ -z "is_a_grade" ]; then
	echo "problem with $i: cannot extract $start_str";
	exit 1;
    fi;
}


get_and_format_reviews()
{
    papid="$1";

    review_proto="$REVIEW_FORM_TEMPLATE$papid";
    mkdir -p "$PAPER_NOTIFICATION_DIR_TEMPLATE$papid";
    count=1;
    for i in `find $REVIEW_DIR -name "$review_proto*" | sort`; do
#	echo "$i";
	#tit_no=`grep -n "TITLE:" "$i" | cut -d ':' -f 1`;
	#auth_no=`grep -n "AUTHORS:" "$i" | cut -d ':' -f 1`;
	PAPER_TITLE=`grep  "TITLE:" "$i" | cut -d ':' -f 2`;
	PAPER_AUTHORS=`grep  "AUTHORS:" "$i" | cut -d ':' -f 2`;
	rev_no=`grep -n "REVIEW:" "$i" | cut -d ':' -f 1`;
	# skip comment for reviewer
	rev_start_no=$(($rev_no+4));
	pccomm_no=`grep -n "REMARKS FOR THE PROGRAMME COMMITTEE:" "$i" | cut -d ':' -f 1`;
	rev_end_no=$(($pccomm_no - 1 - $rev_start_no));

        ## Compute all grades.
	GRADE="";
	extract_grade "OVERALL EVALUATION" "REVIEWER'S CONFIDENCE" "6" "$i";
	overall_grade="$GRADE";
	extract_grade "REVIEWER'S CONFIDENCE" "POTENTIAL FOR LIVE" "6" "$i";
	confidence_grade="$GRADE";
	extract_grade "POTENTIAL FOR LIVE" "TECHNICAL SOUNDNESS" "4" "$i";
	potential_grade="$GRADE";
	extract_grade "TECHNICAL SOUNDNESS" "END" "5" "$i";
	soundness_grade="$GRADE";
	review=`tail -n +"$rev_start_no" "$i" |head -n "$rev_end_no"`;

	filename="$PAPER_NOTIFICATION_DIR_TEMPLATE$papid/$REVIEW_FORM_TEMPLATE$count.txt";
	echo "==============================================================================" > "$filename";
	echo "TITLE: $PAPER_TITLE" >> "$filename";
	echo "GRADES: Overall = $overall_grade (scale: -2 to 3) ; Reviewer's confidence =  $confidence_grade (scale: 1 to 4); Technical soundness = $soundness_grade (scale: 1 to 4)" >> "$filename";
	echo "==============================================================================" >> "$filename";
	echo "Review: "  >> "$filename";
	echo "$review" >> "$filename";
	echo "" >> "$filename";
	echo "==============================================================================" >> "$filename";
	echo "For your information, the grades scale were:
OVERALL EVALUATION:
3 (strong accept)
2 (accept)
1 (weak accept)
-1 (weak reject)
-2 (reject)

REVIEWER'S CONFIDENCE:
4 (expert)
3 (high)
2 (medium)
1 (low)
0 (null)

TECHNICAL SOUNDNESS
4 (bullet proof)
3 (minor problems)
2 (ok, but some problems need to be addressed)
1 (flawed)
" >> "$filename";

	count=$(($count+1));
    done;


}

while read n; do
    ## skip comments.
    is_comment=`echo "$n" | grep '^[ \t]*#'`;
    if ! [ -z "$is_comment" ]; then continue; fi;

    paper_id=`echo "$n" | cut -d ':' -f 1`;
    if [ "$paper_id" -lt 10 ]; then
	paper_id="0$paper_id";
    fi;
    decision=`echo "$n" | cut -d ':' -f 2`;
    author_emails=`echo "$n" | cut -d ':' -f 3`;
    shepherd1=`echo "$n" | cut -d ':' -f 4-`;
    shepherd2="";
    multi_shep=`echo "$shepherd1" | grep ":"`;
    if ! [ -z "$multi_shep" ]; then
	## No more than 2 shepherds.
	shepherd2=`echo "$shepherd1" | cut -d ':' -f 2`;
	shepherd1=`echo "$shepherd1" | cut -d ':' -f 1`;
    fi;

    ## Get the reviews, and format them.
    get_and_format_reviews "$paper_id";
    if [ "$decision" = "A" ]; then mail="$ACCEPT_template"; fi;
    if [ "$decision" = "CA" ]; then mail="$CONDACCEPT_template"; fi;
    if [ "$decision" = "R" ]; then mail="$REJECT_template"; fi;
    filename="$PAPER_NOTIFICATION_DIR_TEMPLATE$papid/notification.txt";
    cp "$mail" "$filename";
    sed -e "s/PAPER_AUTHORS_EMAIL/$author_emails/g" "$filename" > __tmp1;
    mv __tmp1 "$filename";
    sed -e "s/PAPER_TITLE/$PAPER_TITLE/g" "$filename" > __tmp1;
    mv __tmp1 "$filename";
    sed -e "s/PAPER_AUTHORS/$PAPER_AUTHORS/g" "$filename" > __tmp1;
    mv __tmp1 "$filename";
    if ! [ -z "$multi_shep" ]; then
	sed -e "s/SHEPHERDS_NAMES/$shepherd1 and $shepherd2/g" "$filename" > __tmp1;
    else
	sed -e "s/SHEPHERDS_NAMES/$shepherd1/g" "$filename" > __tmp1;
    fi;
    mv __tmp1 "$filename";


done < "$DECISION_FILE"

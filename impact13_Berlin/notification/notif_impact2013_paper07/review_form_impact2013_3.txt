==============================================================================
TITLE:  On Demand Parametric Array Dataflow Analysis
GRADES: Overall = -1 (scale: -2 to 3) ; Reviewer's confidence =  2 (scale: 1 to 4); Technical soundness = (1?) (scale: 1 to 4)
==============================================================================
Review: 

--- Summary:

The topic addressed in this paper is important: extending the scope of
polyhedral optimizations, here for dependence analysis, to non-static
programs. The FADA library already exists that can handle different types of
dynamicity. In this paper, an alternative approach is proposed that is, as
explained by the authors, not to make it more powerful, but to make it faster
and lighter in the use of parameters (that encapsulate, in some way,
dynamicity).

--- Strengths:

The technique has been implemented in a prototype, and compared to FADA, for
all FADA test cases that this new approach can handle (i.e., without the
non-static affine index expressions). It does show what the authors expected:
it is in general faster, it introduces fewer parameters, and the size of the
solutions seem smaller. However, as mentioned by the authors, it is not clear
what is the level of optimizations (in particular simplifications) in FADA
compared to this new approach, thus the size of the solutions is maybe due to
the implementation more than to the method. Also, since parameters do not have
the same meaning (as stated by the authors), maybe the comparison is not
meaningful either. It also depends on how the solutions are being exploited
afterwards, maybe one representation is more interesting than the other (this
is not discussed).

--- Weaknesses:

The technique relies on the notion of filters, filter access relations and
filter value relations, as well as some parameters beta and
lambda. Unfortunately, I failed to understand the semantics of these filters
and how they can be interpreted and used in the final solutions. This is also
due to a heavy use of mathematical notations with too few intuitive
explanations. There are also many obscure sentences (that are maybe accurate
but if the exact meaning of words is understood), that are sometimes important
for the general discussion but that are also sometimes digressions for
subtleties that can be understood only after the big picture is
understood. Even if there is no reason to doubt that the method is correct and
well implemented, I would be unable to implement it myself from the paper
description. The authors should not go that fast at the beginning, they should
state precisely the type of codes that they can address, give the exact
procedure to apply, and proves it rather than enumerating particular
situations. Also, it should be very clear where hypotheses are used for
correctness or feasibility. For example, the top of Page 7 gives a quick
explanation why dynamic index expressions are *not* supported. It would be
better to indicate in the first 6 pages how the fact that index expressions are
*not* dynamic is exploited.

Also, applications are missing. What is the output format? How can the
remaining parameters be used and/or eliminated, interpreted? How can the output
be used for code optimizations? Comparison with other approaches such as array
region analysis should be part of the related work too.

In conclusion, the paper is maybe very deep and interesting. But it should be
rewritten to make it more accessible.

--- Detailed comments:

The title is confusing: it could be understood as an array data flow analysis
on demand. But, as far as I understood, what is "on demand" is the introduction
of parameters, not the analysis itself.

Section 2
=========

Page 2: while loops and dynamic index expressions are said to be "briefly
discussed in Section 7", so the reader expects that they are supported but that
the technique, more complicated, is only sketched. However, the goal of Section
7 is rather to explain that they are not supported. Make this clearer at the
beginning. Also, listing 2 has a while loop, so it is partly supported?

Page 2: why mentioning schedules at the beginning if they are not used in
this paper?

Section 3
=========

Notations start already to be heavy. I am not sure to understand the formula
with the identify function (maybe this is only an implementation issue with
ISL). Also what are the notations with arrows (underlined)? What is a
"nested copy of a relation"? Mention that the proposed ordering is the same as
in Feautrier's seminal paper. Explain also how different statements (i.e.,
different P) are combined. Although this section is quite clear, I did not see
the direct link with the remaining sections. Also, FADA should be explained.

Section 4
=========

Top of Column 1, Page 3:

There are some undefined notations. What does F_i subset I_L -> (I_L -> A)
mean?  That F_i assigns to an iteration a function from iterations to accesses?
Is F_i(k) a function that assigns to an iteration i an access a, or is it just
the pair (k->a)? This is not clear neither through this definition with arrows,
nor through its use in Equation 3, as Nu(F_j(k)) seems to be Nu(k->a) and not
Nu of a function. And Nu is not defined. Similarly, V subset I -> Z^{n} does
not say how Z^{n} is interpreted (it seems afterwards that these are Booleans,
not integers). And what do these n values mean? Also, in Equation 3, the
semantics of the symbol Pi is not defined (and it does not seem to be a
product). Just after, the symbol "dom" (underlined with arrow) is also
undefined. This part should be rewritten with a more accurate description of
what a filter is, mathematically, and what its semantics is. Otherwise, there
is no way to understand the rest of the paper (at least for me).

Page 3, End of Column 1:

If the technique is equivalent to the introduction of arrays to store results
of conditionals, maybe it would be easier to really introduce these arrays in
the code, at least to explain the mechanisms, and possibly to show at the end
that this introduction can be emulated. The link between Equations 4-5 and
Equation 3 is not clear. For Listing 2, the authors seem to introduce a new
counter "i". When is it introduced? What is its semantics? Is it always
possible to do so for general codes? What are the required hypotheses?

Page 3, Column 2:

"This information can be obtained by applying dataflow analysis on the arrays
accessed by the filters [...] then the original filter access relations can be
replaced by a composition with these dependence relations". It is not clear why
there is no conceptual cycle in this strategy, in other words, explain why the
dataflow analysis of these new arrays do not need filters themselves or
preliminary information from other new arrays, which would imply that some
ordering of analysis is needed (and if there is a cycle, it would be
unfeasible). Again, maybe the introduction of the new arrays in the code would
show that there is indeed two levels of resolution. But so far, this is not
clear.

Page 3, column 2:

"in principle, the meaning of Nu depends on whether...". Actually, the reader
is still waiting for the meaning of Nu. Also, this paragraph is too subtle at
this point of discussion to be understood. Or this should be given from the
start when defining precisely what Nu is.

Section 5
=========

Page 4, column 1:

"The exact form and meaning of the parameters depends on whether we are
considering the final result or intermediate results". Intermediate of what? 
And again, as for Nu, the meaning of something cannot be given on the fly, in a
such vague way. Please give a formal and clear definition of what these
parameters mean.

Are Equations 11 and 12 definitions? Ways to compute parameters or manipulate
them?

The sentence "the dependence of the access C to {state} from the statement So
on the access P to the same variable from statement C" is too obscure to
me. Make several sentences. "There is only a dependence ... if the last
execution was on the previous iteration": I don't understand. What are the
dependences in this code? Why not a dependence from two iterations earlier if
the execution goes in the "if branch", then twice in the "else branch"?

"We also know that the first l iterations ... and that the same property holds
for the previously considered maximization problems with the current dataflow
problem": sorry, there is no way I can understand this. Please give more
explanations, especially because this is maybe a key part to understand the
semantics and use of parameters. What are "the initial iterators of k", initial
with respect to what? Similarly, the end of this section is impossible to
follow.

Page 4, column 2:

"There is however no interaction between different sink iterations": why? And
what is an "interaction"?

Section 6
=========

"In principle, the result is that either parametrization is required, or it is
not required" is a truism.

"as they would only lead to conflicts": conflicts of what?

Explain Equation 13. Is "l" the same "l" as before? How can sigma be < 0
(later), what does it mean?

There is no way I can understand this section. "If so, we use he constraints on
those parameters and the filters of the associated (other) potential sources to
derive extra information" is too vague. Same for the end of this paragraph.
What does "We pull back the filter access relations over M1" mean? "There is
also no need for all filter access relations on both sides to match". Both
sides of what? Why would we think they should match?

End of Section 6.3: finally, what is the outcome of the analysis? What do we do
with it?

Section 7
=========

Limitations compared to FADA for example should have been given earlier. While
loops are considered or not? Listing 2 has a while loop.

Section 8
==========

How was the correctness of results checked in Table 1, in particular how did
the authors detect that FADA gives two erroneous results? As parameters do not
seem to have the same semantics, it may be hard to check.

"This is made possible by a judicious definition of these parameters". But what
is this definition?

==============================================================================
For your information, the grades scale were:
OVERALL EVALUATION:
3 (strong accept)
2 (accept)
1 (weak accept)
-1 (weak reject)
-2 (reject)

REVIEWER'S CONFIDENCE:
4 (expert)
3 (high)
2 (medium)
1 (low)
0 (null)

TECHNICAL SOUNDNESS
4 (bullet proof)
3 (minor problems)
2 (ok, but some problems need to be addressed)
1 (flawed)


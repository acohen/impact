==============================================================================
TITLE:  On the Scalability of Loop Tiling Techniques
GRADES: Overall = -2 (scale: -2 to 3) ; Reviewer's confidence =  4 (scale: 1 to 4); Technical soundness = 3 (scale: 1 to 4)
==============================================================================
Review: 

--- Summary:

The paper discusses the issue of weak scalability of different
approaches to tiling. The main point is that the approach to tiling
implemented in many existing polyhedral compilation frameworks such as
Pluto, Pocc, Chill etc. utilizes "tile wavefront" parallelism for
stencil computations. This is because these systems perform
"rectangular" tiling in a suitably transformed iteration space. With
stencil computations, skewing with respect to the time dimension is
required to make rectangular tiling along all dimensions valid. But
inter-tile dependences along the spatial dimensions in the stencil
code after "time skewing" mean that only pipelined or wavefront
parallelism can be exploited.

Alternative approaches to tiling have been known that avoid the loss
of tile-level parallelism with rectangular tiling in the skewed
iteration space, but until the very recent work (ref [3] in the
paper), there was no known fully general implementation of any of
those alternatives.

The paper provides a good explanation of this problem.


--- Strengths:

The paper is clearly written.

--- Weaknesses:

There is no technical contribution in this paper. Every other
submission to Impact-13 presents some new technical contribution, in
many cases along with experimental performance data.

--- Detailed comments:

The paper documents an issue that is well known to the community.
The paper might have been more interesting to IMPACT if experimental
data comparing different tiling techniques were provided or some new
insights were provided into trade-offs between the different
techniques. For example, effective SIMD vectorization might be more
challenging with diamond tiling than rectangular tiling.

==============================================================================
For your information, the grades scale were:
OVERALL EVALUATION:
3 (strong accept)
2 (accept)
1 (weak accept)
-1 (weak reject)
-2 (reject)

REVIEWER'S CONFIDENCE:
4 (expert)
3 (high)
2 (medium)
1 (low)
0 (null)

TECHNICAL SOUNDNESS
4 (bullet proof)
3 (minor problems)
2 (ok, but some problems need to be addressed)
1 (flawed)


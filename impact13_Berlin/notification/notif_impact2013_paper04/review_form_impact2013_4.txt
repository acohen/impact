==============================================================================
TITLE:  Chernikova-less compilation part I
GRADES: Overall = -1 (scale: -2 to 3) ; Reviewer's confidence =  2 (scale: 1 to 4); Technical soundness = 3 (scale: 1 to 4)
==============================================================================
Review: 

--- Summary:

This paper presents a new algorithm to compare to parametric polyhedra at dimension l. This is a key step in Quillere's code generation algorithm. Their new algorithm does not use Chernikova's algorithm, which is assumed to be reason for the high complexity of Quillere's algorithm.

--- Strengths:

Complexity comparison of two algorithms.

--- Weaknesses:

Section 2.1 fails to show that Chernikova's algorithm is an intrinsic building block for Quillere's method. Chernikova's algorithm is used indirectly to perform projections and to test a polyhedron for emptiness.

No related work with recent publications about code generation for the polyhedral model.

--- Detailed comments:

Section 2: n_A vs N_B

Section 4, Theorem 1: quantifiers for l and m?

Proof: *positive* linear combination?

Theorem 2: quantifiers?

can't -> cannot

The title could be more specific.

==============================================================================
For your information, the grades scale were:
OVERALL EVALUATION:
3 (strong accept)
2 (accept)
1 (weak accept)
-1 (weak reject)
-2 (reject)

REVIEWER'S CONFIDENCE:
4 (expert)
3 (high)
2 (medium)
1 (low)
0 (null)

TECHNICAL SOUNDNESS
4 (bullet proof)
3 (minor problems)
2 (ok, but some problems need to be addressed)
1 (flawed)


To:
Reply-To: impact-chairs@lists.gforge.inria.fr
Subject: Call for Papers: IMPACT 2013 -- Extended deadline

*** Apologies if you receive multiple copies. ***
*** Please forward to interested colleagues.  ***


** Important: submission deadline extended to November 2, 2012

This is a CALL FOR PAPERS for the
3rd International Workshop on Polyhedral Compilation
Techniques (IMPACT 2013)
http://impact.gforge.inria.fr/impact2013/
Berlin, Germany, January 21, 2013

in conjunction with HiPEAC 2013 (Jan 21-23, 2013).


======================================
IMPORTANT DATES:
Submission deadline: November 02, 2012 (extended)
Author notification: November 30, 2012
Final version due:   December 10, 2012
======================================

OVERVIEW:
With ubiquitous multicore processors and the increasing role of
hardware accelerators, polyhedral compilation techniques have
gained a lot of attention in both academia and industry.
Polyhedral compilation provides a homogeneous framework to design
effective optimizations for high performance computing, addressing
coarse-grain and fine-grain parallelism, distributed- and
shared-memory parallelism. IMPACT is a unique workshop focusing
exclusively on polyhedral compilation technologies, bringing
together researchers and practitioners for a high-quality
one-day event including keynote, technical paper presentations,
and panel discussions.

We welcome both theoretical and experimental papers on all aspects of
polyhedral compilation and optimization. We also welcome submissions
describing preliminary results, crazy new ideas, position papers,
experience reports, and available tools, with an aim to stimulate
discussions, collaborations, and advances in the field.

Topics of interest include, but are not limited to:
 - program optimization (automatic parallelization, tiling, etc.)
 - code generation
 - data/communication management on GPUs, accelerators and
   distributed systems
 - hardware/high-level synthesis for affine programs
 - static analysis
 - program verification
 - model checking
 - theoretical foundations of the polyhedral model
 - extensions of the polyhedral model
 - scalability and robustness of polyhedral compilation techniques
 - tool demonstration


Keynote speaker (confirmed): Albert Cohen (INRIA, France)



SUBMISSION:
Submissions should not exceed 8 pages (recommended 6 pages)
formatted as per ACM proceedings format.  Please use the following
template when preparing your manuscript:
http://www.acm.org/sigs/publications/proceedings-templates

Submissions should be in PDF format and printable on US Letter or
A4 sized paper.  Please send your submission by the deadline to:
impact-chairs@lists.gforge.inria.fr

Please indicate in your email if you wish to submit as a:
 (1) regular paper
 (2) position paper
 (3) wild and crazy idea
 (4) tool demonstration

Proceedings will be published online and distributed to the
participants. Publication at IMPACT will not prevent later
publication in conferences or journals of the presented work.




COMMITTEES:

Organizers and Program Chairs:
Armin Groesslinger (University of Passau, Germany)
Louis-Noel Pouchet (University of California Los Angeles, USA)
Contact: impact-chairs@lists.gforge.inria.fr

Program Committee:
Christophe Alias (ENS Lyon, France)
Cedric Bastoul (INRIA, France)
Uday Bondhugula (IISc, India)
Philippe Clauss (University of Strasbourg, France)
Albert Cohen (INRIA, France)
Alain Darte (ENS Lyon, France)
Paul Feautrier (ENS Lyon, France)
Martin Griebl (University of Passau, Germany)
Sebastian Hack (Saarland University, Germany)
Francois Irigoin (MINES ParisTech, France)
Paul Kelly (Imperial College London, UK)
Ronan Keryell (Wild Systems/Silkan, USA)
Vincent Loechner (University of Strasbourg, France)
Benoit Meister (Reservoir Labs, Inc., USA)
Sanjay Rajopadhye (Colorado State University, USA)
P. Sadayappan (Ohio State University, USA)
Michelle Mills Strout (Colorado State University, USA)
Nicolas Vasilache (Reservoir Labs, Inc., USA)
Sven Verdoolaege (KU Leuven/ENS, France)

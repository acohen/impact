
A: Christophe Alias (ENS Lyon, France)
A: Cedric Bastoul (INRIA, France)
A: Uday Bondhugula (IISc, India)
A: Philippe Clauss (University of Strasbourg, France)
A: Albert Cohen (INRIA, France)
A: Alain Darte (ENS Lyon, France)
A: Paul Feautrier (ENS Lyon, France)
A: Martin Griebl (University of Passau, Germany)
A: Sebastian Hack (Saarland University, Germany)
A: Francois Irigoin (MINES ParisTech, France)
A: Paul Kelly (Imperial College London, UK)
A: Ronan Keryell (Wild Systems / Silkan, USA)
A: Vincent Loechner (University of Strasbourg, France)
A: Benoit Meister (Reservoir Labs, Inc., USA)
A: Sanjay Rajopadhye (Colorado State University, USA)
A: P. Sadayappan (Ohio State University, USA)
A: Michelle Strout (Colorado State University, USA)
A: Nicolas Vasilache (Reservoir Labs, Inc., USA)
A: Sven Verdoolaege (KU Leuven/ENS)


D: Mary Hall (University of Utah, USA)

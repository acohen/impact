To:
Reply-To: impact-chairs@lists.gforge.inria.fr
Subject: Call for participation: IMPACT 2013

*** Apologies if you receive multiple copies. ***
*** Please forward to interested colleagues.  ***

CALL FOR PARTICIPATION

3rd International Workshop on Polyhedral
Compilation Techniques (IMPACT 2013)
http://impact.gforge.inria.fr/impact2013/
Berlin, Germany, January 21, 2013

in conjunction with HiPEAC 2013 (Jan 21-23, 2013)


OVERVIEW:
With ubiquitous multicore processors and the increasing role of
hardware accelerators, polyhedral compilation techniques have gained a
lot of attention in both academia and industry. Polyhedral compilation
provides a homogeneous framework to design effective optimizations for
high performance computing, addressing coarse-grain and fine-grain
parallelism, distributed- and shared-memory parallelism. IMPACT is a
unique workshop focusing exclusively on polyhedral compilation
technologies, bringing together researchers and practitioners for a
high-quality one-day event including keynote, technical paper
presentations, and panel discussions. This is the third edition of
IMPACT and is being organized in conjunction with with HiPEAC 2013 in
Berlin, Germany as a full-day workshop.


WORKSHOP PROGRAM:

10:00-11:00 Session I: Keynote

  "Who wants to adopt a polyhedral compiler?"
  Albert Cohen (INRIA, France) 

11:00-11:30  Break

11:30-13:00  Session II: Tiling & Dependence Analysis

  "On the Scalability of Loop Tiling Techniques"
  David G. Wonnacott, Michelle Mills Strout

  "Memory Allocations for Tiled Uniform Dependence Programs"
  Tomofumi Yuki, Sanjay Rajopadhye

  "On Demand Parametric Array Dataflow Analysis"
  Sven Verdoolaege, Hristo Nikolov, Todor Stefanov

13:00-14:00  Break

14:00-15:30  Session III: Parallelism Constructs & Speculation

  "Multifor for Multicore"
  Imen Fassi, Philippe Clauss, Matthieu Kuhn, Yosr Slama

  "Facilitate SIMD-Code-Generation in the Polyhedral Model by
  Hardware-aware Automatic Code-Transformation"
  Dustin Feld, Thomas Soddemann, Michael Jünger, Sven Mallach

  "SPolly: Speculative Optimizations in the Polyhedral Model"
  Johannes Doerfert, Clemens Hammacher, Kevin Streit, Sebastian Hack

15:30-16:00  Break

16:00-17:00  Session IV: Panel discussions (TBD)


Please register for the IMPACT workshop at
http://www.hipeac.net/conference/berlin/registration

The early registration deadline ends on December 21, 2012.


Organizers:
Armin Groesslinger, University of Passau
Louis-Noel Pouchet, University of California Los Angeles

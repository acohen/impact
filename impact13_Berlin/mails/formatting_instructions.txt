Dear IMPACT authors,

As usual with IMPACT, your final papers will be available on the
IMPACT website (and the USB stick distributed at HiPEAC provided that
we receive the final version on time).

In addition, we would like to make a single "proceedings" document
from the final versions of the papers accepted. To be able to compile
this document we kindly ask you to the pay attention to the following
points when preparing the final version of your paper:

- Please do _not_ put page numbers on your paper.

- Please use either of the styles available from

    http://www.acm.org/sigs/publications/proceedings-templates

  We recommend that you use the tighter alternate style (sig-alternate).

- Please hide the "permissions" block on the first page in the
  ACM style, e.g. by putting

    \makeatletter
    \let\@copyrightspace\relax
    \makeatother

  in the preamble of your document
  (see also http://www.acm.org/sigs/publications/sigfaq#a21 ).

- Please make sure that the PDF file is US letter (which is the
  paper format of the ACM classes). One way to make sure that
  pdflatex produces US letter is to use:

    \setlength{\pdfpagewidth}{8.5in}
    \setlength{\pdfpageheight}{11in}

  in the preamble of your document. If you compile via DVI,
  you can use:

    \special{papersize=8.5in,11in}
  
  in the preamble of your document.


In case you have any questions concerning the preparation of the final
version of your paper, do not hesitate to contact us at:
impact-chairs@lists.gforge.inria.fr


Best regards,

Armin & Louis-Noel
 

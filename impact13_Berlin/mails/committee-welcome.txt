Dear all,

Armin and I would like to thank all of you for having accepted to serve
in the PC of the next edition of IMPACT. We are glad that all (but
one) people in our tentative PC list we have shared with you have
accepted to participate. Only Mary Hall has preferred to decline our
invitation this year, as she is already packed with SuperComputing to
be held in Utah. So, we are pleased to have 4 new members this year,
and we welcome back the other members who already participated in the
success of IMPACT last year!

Please find attached a draft of the CfP, that we will
broadcast in the coming days on the standard mailing lists. Please
check your affiliation, to be sure everything is right.  We also would
like to mention that, similarly to the previous years, PC members are
highly encouraged to submit to IMPACT. The reviewing process will be
such that authors that are also PC members will of course not be
included in the email discussions regarding their papers, and Armin
and I will work through the potential conflicts of interests. Please
note also a novelty in for this 3rd edition: we will offer the
possibility to the authors to include their accepted papers in a
common electronic proceedings system that will not prevent
republication and does not require copyright assignment. We are still
working through the details, but U. Passau is offering such system.


Finally, feel free to contact all PC members at once through the
impact-committee@lists.gforge.inria.fr, or only Armin and I through
the impact-chair@lists.gforge.inria.fr mailing list.


Best regards,
Louis-Noël Pouchet and Armin Größlinger

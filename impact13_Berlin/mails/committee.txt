To: ...
Reply-To: impact-chairs@lists.gforge.inria.fr
Cc: impact-chairs@lists.gforge.inria.fr
Subject: IMPACT 2013 Program Committee invitation

Dear <Prospective PC Member>,

We are organizing the third edition of the International Workshop on
Polyhedral Compilation Techniques (IMPACT 2013), to be held in conjunction with
HiPEAC 2013 in Berlin, Germany (January 21-23,
http://www.hipeac.net/conference). 

We would like to invite you to participate to the Program Committee of
IMPACT'13.  The expected work load will be to review a few papers
(likely less than five), and participate to email discussions to
establish the list of accepted papers in November 2012. 

Please let us know if you agree to participate. Please also let us
know in case you would not be able to be on the PC, but would like to
recommend one of your colleagues (from your organization), or if you
have any other suggestions for the PC.

As you may know, the first two editions of this workshop (held in
conjunction with CGO 2011 in Chamonix, France and HiPEAC 2012 in Paris,
France) were remarkably successful (50-70 attendees), see also
http://impact2011.inrialpes.fr
http://impact.gforge.inria.fr/impact2012/

This is the tentative schedule for IMPACT 2013:
- Paper submission deadline: November 02, 2012
- Notification of acceptance: November 30, 2012
- Final version due: December 07, 2012
- Workshop Date: January 21 (tentative), 2013.

We plan to organize paper sessions and a quick tools session with
demos. We plan to accept a maximum of ten papers, and to have one
keynote talk and a panel discussion.

For your information, our tentative PC list is as follows.

Christophe Alias (ENS Lyon, France)
Cedric Bastoul (INRIA, France)
Uday Bondhugula (IISc, India)
Philippe Clauss (University of Strasbourg, France)
Albert Cohen (INRIA, France)
Alain Darte (ENS Lyon, France)
Paul Feautrier (ENS Lyon, France)
Martin Griebl (University of Passau, Germany)
Sebastian Hack (Saarland University, Germany)
Mary Hall (University of Utah, USA)
Francois Irigoin (MINES ParisTech, France)
Paul Kelly (Imperial College London, UK)
Ronan Keryell (HPC Project)
Vincent Loechner (University of Strasbourg, France)
Benoit Meister (Reservoir Labs, Inc., USA)
Sanjay Rajopadhye (Colorado State University, USA)
P. Sadayappan (Ohio State University, USA)
Michelle Strout (Colorado State University, USA)
Nicolas Vasilache (Reservoir Labs, Inc., USA)
Sven Verdoolaege (KU Leuven/ENS)

The preliminary web site for IMPACT 2013 is available at
http://impact.gforge.inria.fr/impact2013/

Thank you for being a part of the success of IMPACT!

Best Regards,

Armin Größlinger and Louis-Noël Pouchet
Program Chairs
IMPACT 2013

=== Paper 01:
*** SUBMISSION NUMBER: 01
*** TITLE: Multifor for Multicore
*** AUTHORS: Imèn Fassi, Philippe Clauss, Matthieu Kuhn, Yosr Slama
*** CATEGORY: wild and crazy
====== Reviewers:
*** REVIEW FORM ID: 01::01
*** PC MEMBER: Paul Feautrier
------
*** REVIEW FORM ID: 03::01
*** PC MEMBER: Cedric Bastoul
------
*** REVIEW FORM ID: 10::01
*** PC MEMBER: Francois Irigoin
------
*** REVIEW FORM ID: 11::01
*** PC MEMBER: Paul Kelly
------
*** REVIEW FORM ID: 16::01
*** PC MEMBER: P. Sadayappan
------
*** REVIEW FORM ID: 17::01
*** PC MEMBER: Michelle Mills Strout



=== Paper 02:
*** SUBMISSION NUMBER: 02
*** TITLE: Memory Access Pattern Analysis for Heterogeneous Platforms and Reconfigurable Architectures
*** AUTHORS: Vassilis Vassiliadis, Muhsen Owaida, Christos D. Antonopoulos, Nikolaos Bellas
*** CATEGORY: position paper
====== Reviewers:
*** REVIEW FORM ID: 02::02
*** PC MEMBER: Christophe Alias
------
*** REVIEW FORM ID: 05::02
*** PC MEMBER: Philippe Clauss
------
*** REVIEW FORM ID: 09::02
*** PC MEMBER: Sebastian Hack
------
*** REVIEW FORM ID: 12::02
*** PC MEMBER: Ronan Keryell
------
*** REVIEW FORM ID: 13::02
*** PC MEMBER: Vincent Loechner



=== Paper 03:
*** SUBMISSION NUMBER: 03
*** TITLE: On the Scalability of Loop Tiling Techniques
*** AUTHORS: David G. Wonnacott, Michelle Mills Strout
*** CATEGORY: position paper
====== Reviewers:
*** REVIEW FORM ID: 04::03
*** PC MEMBER: Uday Bondhugula
-----
*** REVIEW FORM ID: 08::03
*** PC MEMBER: Martin Griebl
-----
*** REVIEW FORM ID: 14::03
*** PC MEMBER: Benoit Meister
-----
*** REVIEW FORM ID: 16::03
*** PC MEMBER: P. Sadayappan
-----
*** REVIEW FORM ID: 19::03
*** PC MEMBER: Sven Verdoolaege



=== Paper 04:
*** SUBMISSION NUMBER: 04
*** TITLE: Chernikova-less compilation part I: Topological sort
*** AUTHORS: Benoit Meister, Nicolas Vasilache, Muthu Baskaran, Richard Lethin
*** CATEGORY: regular
====== Reviewers:
*** REVIEW FORM ID: 01::04
*** PC MEMBER: Paul Feautrier
-----
*** REVIEW FORM ID: 10::04
*** PC MEMBER: Francois Irigoin
-----
*** REVIEW FORM ID: 15::04
*** PC MEMBER: Sanjay Rajopadhye
-----
*** REVIEW FORM ID: 19::04
*** PC MEMBER: Sven Verdoolaege



=== Paper 05:
*** SUBMISSION NUMBER: 05
*** TITLE: Facilitate SIMD-Code-Generation in the Polyhedral Model by Hardware-aware Automatic Code-Transformation
*** AUTHORS: Dustin Feld, Thomas Soddemann, Michael Jünger, Sven Mallach
*** CATEGORY: regular
====== Reviewers:
*** REVIEW FORM ID: 09::05
*** PC MEMBER: Sebastian Hack
-----
*** REVIEW FORM ID: 13::05
*** PC MEMBER: Vincent Loechner
-----
*** REVIEW FORM ID: 14::05
*** PC MEMBER: Benoit Meister
-----
*** REVIEW FORM ID: 15::05
*** PC MEMBER: Sanjay Rajopadhye
-----
*** REVIEW FORM ID: 18::05
*** PC MEMBER: Nicolas Vasilache



=== Paper 06:
*** SUBMISSION NUMBER: 06
*** TITLE: Memory Allocations for Tiled Uniform Dependence Programs
*** AUTHORS: Tomofumi Yuki, Sanjay Rajopadhye
*** CATEGORY: regular
====== Reviewers:
*** REVIEW FORM ID: 04::06
*** PC MEMBER: Uday Bondhugula
-----
*** REVIEW FORM ID: 06::06
*** PC MEMBER: Albert Cohen
-----
*** REVIEW FORM ID: 07::06
*** PC MEMBER: Alain Darte
-----
*** REVIEW FORM ID: 08::06
*** PC MEMBER: Martin Griebl
-----
*** REVIEW FORM ID: 12::06
*** PC MEMBER: Ronan Keryell




=== Paper 07:
*** SUBMISSION NUMBER: 07
*** TITLE: On Demand Parametric Array Dataflow Analysis
*** AUTHORS: Sven Verdoolaege, Hristo Nikolov, Todor Stefanov
*** CATEGORY: regular
====== Reviewers:
*** REVIEW FORM ID: 02::07
*** PC MEMBER: Christophe Alias
-----
*** REVIEW FORM ID: 04::07
*** PC MEMBER: Uday Bondhugula
-----
*** REVIEW FORM ID: 07::07
*** PC MEMBER: Alain Darte
-----
*** REVIEW FORM ID: 11::07
*** PC MEMBER: Paul Kelly
-----
*** REVIEW FORM ID: 18::07
*** PC MEMBER: Nicholas Vasilache



=== Paper 08:
*** SUBMISSION NUMBER: 08
*** TITLE: SPolly: Speculative Optimizations in the Polyhedral Model
*** AUTHORS: Johannes Doerfert, Clemens Hammacher, Kevin Streit, Sebastian Hack
*** CATEGORY: regular
====== Reviewers:
*** REVIEW FORM ID: 01::08
*** PC MEMBER: Paul Feautrier
-----
*** REVIEW FORM ID: 03::08
*** PC MEMBER: Cedric Bastoul
-----
*** REVIEW FORM ID: 05::08
*** PC MEMBER: Philippe Clauss
-----
*** REVIEW FORM ID: 06::08
*** PC MEMBER: Albert Cohen
-----
*** REVIEW FORM ID: 17::08
*** PC MEMBER: Michelle Mills Strout

Subject: [IMPACT'13] Paper review request: paper #XXX

Dear YYY,

Please find attached the submissions to IMPACT'13 we submit to your
careful review. We did our best to accomodate your preferences while
avoiding conflict, nevertheless please let us know immediately if we
have missed a conflict of interest and you should not review one (or
more) paper(s) we have assigned to you.

Reviews are due no later than November 23, 2012, by email to:
impact-chairs@lists.gforge.inria.fr

We also provide you with review forms, one for each paper you have to
review. Please return those forms completed with your review to us by
the deadline.


A few guidelines for the reviewing process:

- IMPACT is a workshop where we welcome novel ideas, position papers
  that provide a different (and possibly provocative) angle on known
  research problems, as well as new technical contributions. Please
  keep an open mind when reviewing your papers, and carefully consider
  the grading of a paper about its potential for live discussions.

- In terms of acceptance rate, our preliminary plan is to have 6
  papers presented during the workshop, out of the 8 submissions we
  have received. However, if all 8 papers are of high quality and/or
  have good potential for live dicussions, we are open to extend to up
  to 8 papers accepted. 

- We are glad to have been able to put together a PC of extremely high
  quality for IMPACT'13. As the review load is limited (2-3 papers per
  PC member), we hope each reviewer will be able to provide a
  comprehensive and detailed review about the submitted paper. We
  would like to mention that the blind review process will be
  preserved, that is a PC member who submitted a paper will not have
  access to the name of the PC members who reviewed her/his
  paper. However, full reviews, including the reviewer name, will be
  made available during the per-paper email discussions. There will be
  one private email thread per paper, where only people not in
  conflict will be invited to read the full reviews and share their
  opinion about the submitted paper.

- Should you need any assistance, have any question about the review
  process or about the submitted papers, need to ask a question to the
  authors, please do not hesitate to contact Armin or Louis-Noel, we
  are at your entire disposal.


Best Regards,
Armin & Louis-Noel
IMPACT 2013

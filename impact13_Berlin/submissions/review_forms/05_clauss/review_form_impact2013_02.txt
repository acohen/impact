--------------------------------------------------------------
*** REVIEW FORM ID: 05::02
*** SUBMISSION NUMBER: 02
*** TITLE: Memory Access Pattern Analysis for Heterogeneous Platforms and Reconfigurable Architectures
*** AUTHORS: Vassilis Vassiliadis, Muhsen Owaida, Christos D. Antonopoulos, Nikolaos Bellas
*** CATEGORY: position paper
*** PC MEMBER: Philippe Clauss
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:


--- Strengths:


--- Weaknesses:


--- Detailed comments:


--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
*** REVIEWER'S FIRST NAME: (write in the next line)

*** REVIEWER'S LAST NAME: (write in the next line)

*** REVIEWER'S EMAIL ADDRESS: (write in the next line)

--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

---   3 (strong accept)
---   2 (accept)
---   1 (weak accept)
---  -1 (weak reject)
---  -2 (reject)

*** REVIEWER'S CONFIDENCE:

---  4 (expert)
---  3 (high)
---  2 (medium)
---  1 (low)
---  0 (null)

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)
---  2 (medium)
---  1 (mild)

*** TECHNICAL SOUNDNESS

---  4 (bullet proof)
---  3 (minor problems)
---  2 (ok, but some problems need to be addressed)
---  1 flawed


*** END
--------------------------------------------------------------

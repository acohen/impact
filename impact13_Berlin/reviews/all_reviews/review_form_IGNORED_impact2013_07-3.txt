--------------------------------------------------------------
*** REVIEW FORM ID: 11::07
*** SUBMISSION NUMBER: 07
*** TITLE: On Demand Parametric Array Dataflow Analysis
*** AUTHORS: Sven Verdoolaege, Hristo Nikolov, Todor Stefanov
*** CATEGORY: regular
*** PC MEMBER: Paul Kelly
--------------------------------------------------------------
*** REVIEW:
---  Please provide a detailed review, including justification for
---  your scores. This review will be sent to the authors unless
---  the PC chairs decide not to do so. This field is required.

--- Summary:

	The paper proposes a series of improvements over existing array dataflow analysis
techniques in the presence of dynamic control behavior. The nature of these improvements
is two-fold. On one hand, a new representation scheme is proposed that admits to
a more efficient manipulation of dynamic parameters while on the other hand, ways
of reducing the amount of symbolic parameters required by the dataflow analysis 
algorithm are formulated. This new approach leads to a reduction in compilation time 
along with clear improvment in scalability as indicated by experimental results. 

	More specifically, the representation of iteration spaces bounded by dynamic
(i.e. run-time) parameters is based on the concept of "iteration space filters"
consisted of "filter access relations" and "filter value relations". Redundant
parameters are then eliminated from the dataflow problem formulation by inspection of
those filters. Finally, the paper discusses extensions to support "while" loops
and how additional constraints can be communicated to assist the analysis.


--- Strengths:

- Writting style
- Solid understanding of dataflow analysis and related work
- Clear motivation and contribution statement
- Highly relevant to the context of the IMPACT workshop

--- Weaknesses:

- Complex notation
- Confusing experimental results


--- Detailed comments:

	Extending the applicability of polyhedral compilation technologies is undoubtly
an intriguing problem that can raise interesting (phylosophical) discussions.
However, it's still not clear to me how these techniques could be translated into
realistic benefits on software performance and this paper does not provide any
insight into that. It would have been helpful if the authors had clearly shown
under which circumstances existing approaches (i.e. FADA) introduce too many
parameters and why is this critical in practice.

	By looking at the experimental results, it is hard to derive any correlation
between compilation time and number of parameters in most benchmarks. This observation
is reinforced by the fact that the level of confidence in the time measurments is
unknown and not discussed at all. Nevertheless, what is clear from the results is
a scalability issue faced by the fadatool with/without the -s option (never explained
what this option means). I believe that
the case of the paper would have been stronger and the readability of the paper
improved, if the authors had based their narrative on that particular advantage instead
of a generic discussion on parameter reduction. 


--------------------------------------------------------------
*** REMARKS FOR THE PROGRAMME COMMITTEE:
---  If you wish to add any remarks for PC members, please write
---  them below. These remarks will only be used during the PC
---  meeting. They will not be sent to the authors. This field is
---  optional.

--------------------------------------------------------------
--- If the review was written by (or with the help from) a
--- subreviewer different from the PC member in charge, add
--- information about the subreviewer in the form below. Do not
--- modify the lines beginning with ***
*** REVIEWER'S FIRST NAME: (write in the next line)
Athanasios

*** REVIEWER'S LAST NAME: (write in the next line)
Konstantinidis

*** REVIEWER'S EMAIL ADDRESS: (write in the next line)
ak807@doc.ic.ac.uk

--------------------------------------------------------------
--- In the evaluations below, uncomment the line with your
--- evaluation or confidence. You can also remove the
--- irrelevant lines
*** OVERALL EVALUATION:

---   2 (accept)


*** REVIEWER'S CONFIDENCE:

---  3 (high)

*** POTENTIAL FOR LIVE QUESTIONS/FEEBACK/FIGHTS

---  3 (hot)

*** TECHNICAL SOUNDNESS

---  3 (minor problems)


*** END
--------------------------------------------------------------

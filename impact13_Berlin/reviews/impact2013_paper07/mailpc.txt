From: impact-chairs@lists.gforge.inria.fr
To: paper07@impact.kalahimbra.net
Reply-To: paper07@impact.kalahimbra.net
Subject: [IMPACT'13] PC discussion for paper #07

Dear PC members,


Please find attached the reviews which have been produced for
paper #07. The grades given to this paper are: 1 2 2 3.
The current grade average for this paper is 2.00 (on a
scale from -2 to 3), and the range of grades goes from 1 to 3.

Our proposed decision for this paper is accept. But we would
like to give everyone the opportunity to voice their opinion, in
particular people who champion this paper, or on the contrary who
strongly believe the paper should be rejected. 

We suggest a few guidelines for this email discussion:

- Please read carefully the reviews which have been provided by other
  PC members. Also, we remind you that all submissions can be
  downloaded from this webpage:
  http://www.cs.ucla.edu/~pouchet/private/impact2013/ 
  login: impact2013 
  password: submissions

- In case you oppose the tentative decision, please provide a
  motivated reasoning to explain why you challenge the other
  review(s).

- In case you agree with the tentative decision, still please let
  everyone know about it.

- In case you do not provide us with feedback through this email
  thread, we will assume you are fine with either accept or reject
  decisions.

- We welcome philosophical and "political" discussions about paper
  acceptance. Feel free to share your point of view with us.

- To participate in the discussion for this paper, please send
  your comments to paper07@impact.kalahimbra.net
  (e.g., by replying to this email). Mails sent to this address
  will be forwarded to PC members not in conflict with this paper
  (and also to the chairs, so there is no need to CC us).


Best regards,

Armin and Louis-Noel

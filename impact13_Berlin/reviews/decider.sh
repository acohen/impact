#!/bin/sh
## decider.sh for  in /Users/pouchet
##
## Made by Louis-Noel Pouchet
## Contact: <pouchet@cs.ucla.edu>
##
## Started on  Sat Nov 24 19:05:45 2012 Louis-Noel Pouchet
## Last update Sun Nov 25 17:52:09 2012 Louis-Noel Pouchet
##


ACCEPT_THRESOLD="1.25";
REJECT_THRESOLD="-1.25";
NB_PAPERS=8;
CONF="impact2013";

ACCEPT="accept";
REJECT="reject";
OTHER="discussed";

EMAIL_TEMPLATE="template_emails";
ACCEPT_EMAIL="$EMAIL_TEMPLATE/accept.txt";
REJECT_EMAIL="$EMAIL_TEMPLATE/reject.txt";
OTHER_EMAIL="$EMAIL_TEMPLATE/discussion.txt";

###
paper="_paper";
for i in `seq 1 $NB_PAPERS`; do
    if [ $i -lt 10 ]; then
	papid="0$i";
    else
	papid="$i";
    fi;
    file="$CONF$paper$papid/summary.txt";
    avg=`grep "AVG_GRADE" "$file" | cut -d ' ' -f 2`;
    status=`echo "$avg $ACCEPT_THRESOLD $REJECT_THRESOLD \"$ACCEPT\" \"$REJECT\" \"$OTHER\"" | awk '{if ($1 >= $2) print $4; else { if ($1 <= $3) print $5; else print $6; } }' | sed -e 's/"//g'`;

    echo "Paper #$papid: $status";

    if [ "$status" = "$ACCEPT" ]; then mail="$ACCEPT_EMAIL"; fi;
    if [ "$status" = "$REJECT" ]; then mail="$REJECT_EMAIL"; fi;
    if [ "$status" = "$OTHER" ]; then mail="$OTHER_EMAIL"; fi;
    cp "$mail" "$CONF$paper$papid/mailpc.txt";
    mail="$CONF$paper$papid/mailpc.txt";
    min=`grep "MIN_GRADE" "$file" | cut -d ' ' -f 2`;
    max=`grep "MAX_GRADE" "$file" | cut -d ' ' -f 2`;
    avg=`grep "AVG_GRADE" "$file" | cut -d ' ' -f 2`;
    avg=`echo "$avg" | sed -e 's/^\.\(.*\)/0\.\1/g'`;
    allgr=`grep "ALL_GRADES" "$file" | cut -d ' ' -f 2-`;

    sed -e "s/CHAIR_DECISION/$status/g" "$mail" > __tmp1;
    sed -e "s/OVERALL_MIN/$min/g" __tmp1 > "$mail";
    sed -e "s/OVERALL_MAX/$max/g" "$mail" > __tmp1;
    sed -e "s/OVERALL_AVG/$avg/g" __tmp1 > "$mail";
    sed -e "s/PAPER_ID/$papid/g" "$mail" > __tmp1;
    sed -e "s/ALL_GRADES/$allgr/g" __tmp1 > "$mail";
    rm -f __tmp1;

done;

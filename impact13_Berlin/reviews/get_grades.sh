#!/bin/sh
## get_grades.sh for  in /Users/pouchet
## 
## Made by Louis-Noel Pouchet
## Contact: <pouchet@cs.ucla.edu>
## 
## Started on  Sat Nov 24 16:37:40 2012 Louis-Noel Pouchet
## Last update Fri Nov 30 14:35:52 2012 Louis-Noel Pouchet
##

REVIEW_DIR="all_reviews";
REVIEW_FORM_PROTOTYPE="review_form_impact2013";
OUTPUT_FILE="grades_impact2013.csv";

#if [ -f /bin/echo ];  then echo_cmd="/bin/echo"; else echo_cmd="echo"; fi;

## Score extractor. Damned those users who cannot follow guidelines!
extract_grade()
{
    start_str="$1";
    end_str="$2";
    num_lines="$3";
    file="$4";

    GRADE="";
    overall=`grep -A $num_lines "$start_str" "$file"`
    has_removed=`echo "$overall" | grep -n "$end_str"`
    if ! [ -z "$has_removed" ]; then
	linenum=`echo "$has_removed" | cut -d : -f 1`;
	linenum=$(($linenum-1));
	piece=`echo "$overall" | head -n "$linenum" | tail -n +2`;

	grade=`echo "$piece" | grep "[0-9\-]" | sed -e 's/---//g' | sed -e 's/ //g' | sed -e 's/\([0-9\-]*\).*/\1/g'`;
    else
	grade=`echo "$overall" | tail -n +3 | grep -v "\-\-\-" | sed -e 's/ //g' | sed -e 's/\([0-9\-]*\).*/\1/g'`;
    fi;
    GRADE="$grade";
    is_a_grade=`echo "$GRADE" | grep "[0-9\-]"`;
    if [ -z "$GRADE" ] || [ -z "is_a_grade" ]; then
	echo "problem with $i: cannot extract $start_str";
	exit 1;
    fi;
}

update_summary()
{
    file="$1";
    grade="$2";

    g=`grep "MIN_GRADE" "$file" | cut -d ' ' -f 2`;
    if [ "$g" -gt "$grade" ]; then
	sed -e "s/MIN_GRADE .*/MIN_GRADE $grade/1" "$file" > __tmp1;
	mv __tmp1 "$file";
    fi;
    g=`grep "MAX_GRADE" "$file" | cut -d ' ' -f 2`;
    if [ "$g" -lt "$grade" ]; then
	sed -e "s/MAX_GRADE .*/MAX_GRADE $grade/1" "$file" > __tmp1;
	mv __tmp1 "$file";
    fi;
    g=`grep "AVG_GRADE" "$file" | cut -d ' ' -f 2`;
    n=`grep "NB_GRADE" "$file" | cut -d ' ' -f 2`;
    avg=`echo "scale=2; ($g * $n + $grade) / ($n + 1)" | bc`;
    allgr=`grep "ALL_GRADES" "$file" | cut -d ' ' -f 2-`;
    sumexpr=$grade
    for s in $allgr; do
	sumexpr="$sumexpr+$s"
    done
    allgr=`echo "$allgr $grade" | tr ' ' "\n" | sort -n | tr "\n" ' ' | sed -e 's/ *$//g'`
    avg=`echo "scale=2; ($sumexpr) / ($n + 1)" | bc`;
    n=$(($n+1));
    sed -e "s/AVG_GRADE .*/AVG_GRADE $avg/1" "$file" > __tmp1;
    mv __tmp1 "$file";
    sed -e "s/NB_GRADE .*/NB_GRADE $n/1" "$file" > __tmp1;
    mv __tmp1 "$file";
    sed -e "s/ALL_GRADES .*/ALL_GRADES $allgr/1" "$file" > __tmp1
    mv __tmp1 "$file";
}



## Set up.
rm -f "$OUTPUT_FILE";
touch "$OUTPUT_FILE";
find . -name "summary.txt" | xargs rm -f

for i in ` find "$REVIEW_DIR" -name "$REVIEW_FORM_PROTOTYPE*" |sort`; do
    ## In-place conversion from dos/windows to Unix.
    perl -pi -e 's/\r\n/\n/g' "$i";
    perl -pi -e 's/\r\n/\n/g' "$i";
    ## Get the paper and reviewer id
    #echo "$i";
    uid=`grep "REVIEW FORM ID" "$i" | cut -d : -f 2,4 | sed -e 's/ //g'`
    papid=`grep "SUBMISSION NUMBER" "$i" | cut -d : -f 2 | sed -e 's/ //g'`

    ## Copy the review file in the appropriate directory.
    mkdir -p "impact2013_paper$papid";
    cp "$i" "impact2013_paper$papid";

    ## Compute all grades.
    GRADE="";
    extract_grade "OVERALL EVALUATION" "REVIEWER'S CONFIDENCE" "6" "$i";
    overall_grade="$GRADE";
    extract_grade "REVIEWER'S CONFIDENCE" "POTENTIAL FOR LIVE" "6" "$i";
    confidence_grade="$GRADE";
    extract_grade "POTENTIAL FOR LIVE" "TECHNICAL SOUNDNESS" "4" "$i";
    potential_grade="$GRADE";
    extract_grade "TECHNICAL SOUNDNESS" "END" "5" "$i";
    soundness_grade="$GRADE";

    ## Update the paper summary.
    summary_file="impact2013_paper$papid/summary.txt";
    if ! [ -f "$summary_file" ]; then
	echo "MIN_GRADE $overall_grade" >> "$summary_file";
	echo "MAX_GRADE $overall_grade" >> "$summary_file";
	echo "AVG_GRADE $overall_grade" >> "$summary_file";
	echo "NB_GRADE 1" >> "$summary_file";
	echo "ALL_GRADES $overall_grade" >> "$summary_file";
    else
	update_summary "$summary_file" "$overall_grade";
    fi;
    
    ## Check if this paper has been graded already.
    has_grade=`grep "^$papid" "$OUTPUT_FILE"`;
    if [ -z "$has_grade" ]; then
	## No, add grade.
	echo "$papid $overall_grade $confidence_grade $potential_grade $soundness_grade" >> "$OUTPUT_FILE";
    else
	## Yes, append grade.
	sed -e "s/\($papid .*\)/\1 $overall_grade $confidence_grade $potential_grade $soundness_grade/1" "$OUTPUT_FILE" > __tmp.txt;
	mv __tmp.txt "$OUTPUT_FILE";
    fi;

    
done;

Paul H J Kelly graduated in Computer Science from University College London in
1983, and moved to Westfield College, University of London, for his PhD. He
came to Imperial College, London, in 1986, working on fault-tolerant
wafer-scale multicore architectures, and parallel functional programming. He
was appointed as Lecturer in 1989, and Professor of Software Technology in
2009. He leads Imperial's Software Performance Optimisation research group, and
he is also co-Director of Imperial's Centre for Computational Methods in
Science and Engineering. He has worked on single-address-space operating
systems, scalable cache coherency protocols, bounds checking, pointer analysis,
graph algorithms, performance profiling, and custom floating-point arithmetic.
His major current projects include Firedrake, PyOP2 and SLAMBench.

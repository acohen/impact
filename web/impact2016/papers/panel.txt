The polyhedral techniques are known to have complexity issues, as well as
limited scope, in exchange for its exact analysis and robust transformation
framework. There are work on expanding the model to handle more general cases,
as well as on restricting the model to cope with scalability issues. The main
question of this panel is to discuss when the strong capabilities are necessary. 

The key questions we ask are:
- What is the polyhedral model for you? What are the key features/concepts?
- When are the polyhedral techniques overkill? Are there situations where the
  key features are underutilized, or when simpler methods work well?

This is a CALL FOR PAPERS for

    IMPACT 2021

The 11th International Workshop on Polyhedral Compilation Techniques

https://acohen.gitlabpages.inria.fr/impact

Held in conjunction with HiPEAC 2021 (Jan 18-20, 2021), Online Event

============================================
IMPORTANT DATES:
Paper submission:    December 10, 2020 (AoE)
Author notification: January   6, 2021
Final version due:   January  18, 2021
============================================

OVERVIEW:

With multicore processors, hardware accelerators, and deep memory
hierarchies remaining the main sources of performance and
energy-efficiency, polyhedral compilation techniques receive more
attention than ever from both researchers and practitioners.  Thanks
to a unified formalism for parallelization and memory optimization,
these techniques are now powering domain-specific language compilers
yielding best performance in highly competitive areas such as machine
learning and numeric simulation. IMPACT is a unique event focusing
exclusively on polyhedral compilation that brings together researchers
and practitioners for a high-quality one-day event including a keynote
and selected paper presentations.

Since this will be the first entirely online IMPACT, we are also
considering making significant changes to the schedule, and adding
original events such as an Ask-me-Anything session and lightning talks
about work in progress. We are waiting for the HiPEAC organizers to
tell us more about the video-conferencing and networking options, and
are also preparing our own solutions.

We welcome both theoretical and experimental papers on all aspects of
polyhedral compilation and optimization. We also welcome submissions
describing preliminary results, crazy new ideas, position papers,
experience reports, and available tools, with an aim to stimulate
discussions, collaborations, and advances in the field. The following
illustrate potential IMPACT papers:

- Thorough theoretical discussion of a preliminary idea with an attempt to
  place it in context but no experimental results.

- Experimental results comparing two or more existing ideas, followed by a
  detailed analysis.

- Presentation of an existing idea in a different way, including
  illustrations of how the idea applies to new use cases, code,
  architectures, etc.  Attribution should as clear as possible.


Topics of interest include, but are not limited to:
- program optimization (automatic parallelization, tiling, etc.);
- code generation;
- data/communication management on GPUs, accelerators and distributed
  systems;
- hardware/high-level synthesis for affine programs;
- static analysis;
- program verification;
- model checking;
- theoretical foundations of the polyhedral model;
- extensions of the polyhedral model;
- scalability and robustness of polyhedral compilation techniques.


SUBMISSION:

Submissions should not exceed 10 pages (recommended 8 pages), excluding
references, formatted as per ACM SIGPLAN proceedings format.  Please use
version 1.54 or above of the following templates to prepare your
manuscript:

  https://www.acm.org/publications/proceedings-template

Make sure to use the "sigplan" subformat.
Please refer to

  http://sigplan.org/Resources/Author/

for further information on SIGPLAN manuscript formatting.
NOTE: older versions of the article template use smaller fonts for
submission, please double-check that you are using the recent style file
(in particular, various LaTeX distributions ship older versions of the
acmart style file, please download the most recent one from ACM).

Submissions should use PDF format and be printable on US Letter or A4
paper.  Please submit your manuscripts through EasyChair:

  https://easychair.org/conferences/?conf=impact2021

Proceedings will be posted online. If the final version of an accepted
paper does not sufficiently address the comments of the reviewers, then
it may be accompanied by a note from the program committee. Publication
at IMPACT will not prevent later publication in conferences or journals
of the presented work.  However, simultaneous submission to IMPACT and
other workshop, conference, or journal is often prohibited by the policy
of other venues. For instance, a manuscript overlapping significantly
with IMPACT submission cannot be submitted to PLDI 2021 or any other
overlapping SIGPLAN event.

Please make sure that at least one of the authors can attend the
workshop if your work is accepted.

ORGANIZERS and PROGRAM CHAIRS:

‪Aravind Sukumaran Rajam‬ (Washington State University)
Albert Cohen (Google)
Contact: impact-chairs@lists.gforge.inria.fr

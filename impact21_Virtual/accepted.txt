William Moses, Lorenzo Chelini, Ruizhe Zhao and Oleksandr Zinenko. Polygeist: Affine C in MLIR.

Angshuman Parashar, Prasanth Chatarasi and Po-An Tsai. Hardware Abstractions for targeting EDDO Architectures with the Polyhedral Model.

Patrice Quinton and Tomofumi Yuki. Representing Non-Affine Parallel Algorithms by means of Recursive Polyhedral Equations.

Sanjay Rajopadhye. Simplifying Dependent Reductions.

Adithya Dattatri and Benoit Meister. Static Versioning in the Polyhedral Model.

Sven Verdoolaege, Oleksandr Zinenko, Manjunath Kudlur, Ron Estrin, Tianjiao Sun and Harinath Kamepalli. A Templated C++ Interface for isl.

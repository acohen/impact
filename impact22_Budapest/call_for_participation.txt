This is a CALL FOR PARTICIPATION for

    IMPACT 2022

10th International Workshop on Polyhedral Compilation Techniques

https://acohen.gitlabpages.inria.fr/impact/impact2022/

held in conjunction with HiPEAC 2022 in
Budapest, Hungary.

========================================
IMPORTANT DATES:
Hipeac Conference:   January 17-19, 2022
Impact workshop:     January 17, 2022
========================================


*** PROGRAM ***


*** COMMITTEES ***

COMMITTEES:

Organizers and Program Chairs:
Benoit Meister	(Reservoir Labs, USA)
Riyadh Baghdadi	(New York University, UAE)
Contact: impact-chairs@reservoir.com


Program Committee:

Christophe Alias       INRIA & Ecole Normale Superieure de Lyon, France
Cédric Bastoul         Huawei, France
Protonu Basu           Facebook, USA
Somashekaracharya Bhaskaracharya NVIDIA, USA
Jeronimo Castrillon    TU Dresden, Germany
Philippe Clauss        INRIA & U. of Strasbourg, France
Albert  Cohen          Google, France
Paul    Feautrier      Ecole Normale Superieure de Lyon, France
Tobias  Grosser        U. of Edinburgh, UK
Armin   Größlinger     U. of Passau, Germany
Mary    Hall           U. of Utah, USA
Francois Irigoin       Mines ParisTech, France
Paul H J Kelly         Imperial College, London, UK
Martin  Kong           U. of Oklahoma, USA
Sriram  Krishnamoorthy Google, USA
Michael Kruse          Argonne National Laboratory, USA
Louis-Noel Pouchet     Colorado State University, USA
Benoit Pradelle        Xilinx, Germany
Sanjay Rajopadhye      Colorado State University, USA
Ponnuswamy Sadayappan  U. of Utah, USA
Adilla Susungi         Huawei, France
Sanket Tavarageri      Microsoft, India
Ramakrishna Upadrasta  IIT-Hyderabad, India
Sven Verdoolaege       Cerebras Systems, Belgium
Jie Zhao               Ecole Normale Superieure, France
Oleksandr Zinenko      Google, France

*** SPONSORS ***

(Apologies for multiple copies)

             *** CALL FOR PAPERS - IMPACT 2011 ***

First International Workshop on Polyhedral Compilation Techniques

               April 3, 2011, Chamonix, France
                 In conjunction with CGO 2011
              http://impact2011.inrialpes.fr/home


The polyhedral model is increasingly recognized as a fundamental
framework to design efficient, fine-grain, source-level compiler
analysis and optimizations. The workshop on polyhedral compilation
techniques provides a high-quality forum for researchers and
practitioners working on program analysis, transformations and
optimizations in the polyhedral model.  Both theoretical and
experimental papers are welcome, on topics ranging from historical HPC
computing and signal processing to hardware synthesis and new
application domains. We strongly encourage submissions describing
preliminary ideas and results, position papers, experience reports,
and tools effectively available, with the aim to stimulate discussions
and prospective collaborations.

IMPACT aims to be a key event for the polyhedral compilation community
to exchange or to debate ideas and visions. The Program Committee will
value the meeting itself as much as the quality of the papers. To
ensure the workshop is not closed to any interesting emerging ideas,
neither original material nor impressive speedups are required for
submission.


Topics of interest include, but are not limited to:

   * Polyhedral Compilation Flow
   * Automatic Parallelization and Optimization
   * Hardware Synthesis
   * Static Analysis
   * Program Verification
   * Model Checking
   * Robustness and Scalablility of Polyhedral Techniques
   * Theoretical Foundations of the Polyhedral Model
   * Extensions of the Polyhedral Model
   * Tool Demonstration


Important Dates

   * Submission deadline: February 7, 2011

   * Notification to authors: February 28, 2011

   * Final version due: TBD

   * Workshop: April 3, 2011, in conjunction with CGO 2011


Submission

Submissions may not exceed 6 pages (recommended 4-6 pages) formatted
according to the ACM proceedings format. Submissions should be in PDF
and printable on US Letter and A4 sized paper. The submission procedure
will be available on the web site in due time.

Papers will be refereed by the Program Committee and if accepted, and
if the authors wish, will be made available on the workshop web site.


Organizing Committee & Program Chairs

Christophe Alias     INRIA, France (chair)
C�dric Bastoul       U. of Paris-Sud, France (co-chair)


Program Committee

Muthu Baskaran       Reservoir Labs, Inc., USA
Uday Bondhugula      IISc Bangalore, India
Philippe Clauss      U. of Strasbourg, France
Albert Cohen         INRIA, France
Paul Feautrier       ENS Lyon, France
Armin Gr��linger     U. of Passau, Germany
Ronan Keryell        HPC Project, France
Sebastian Pop        AMD, USA
L. Renganarayana     IBM, USA
S. Rajopadhye        Colorado State University, USA
J. Ramanujam         Louisiana State University, USA
P. Sadayappan        The Ohio State University, USA

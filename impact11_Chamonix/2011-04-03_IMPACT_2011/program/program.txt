First International Workshop on Polyhedral Compilation Techniques
(IMPACT 2011)
April 3, 2011, Chamonix, France

(in conjunction with CGO 2011)

The polyhedral model is increasingly recognized as a fundamental framework
to design efficient, fine-grain, source-level compiler analysis and
optimizations. The workshop on polyhedral compilation techniques provides
a high-quality forum for researchers and practitioners working on program
analysis, transformations and optimizations in the polyhedral model. Both
theoretical and experimental papers are welcome, on topics ranging from
classical HPC computing and signal processing to hardware synthesis and
new application domains. We strongly encourage submissions describing
preliminary ideas and results, experience reports, and tools effectively
available, with the aim to stimulate discussions and prospective
collaborations. Position papers are very welcome.

IMPACT aims to be a key event for the polyhedral compilation community to
exchange or to debate ideas and visions. The Program Committee will value
the meeting itself as much as the quality of the proceedings. To ensure the
workshop is not closed to any interesting emerging ideas, neither original
material nor impressive speedups are required for submission.

------------------------------------------

Topics of Interest

Topics of interest include, but are not limited to:

- Polyhedral Compilation Flow
- Automatic Parallelization and Optimization
- Hardware Synthesis
- Static Analysis
- Program Verification
- Model Checking
- Robustness and Scalablility of Polyhedral Techniques
- Theoretical Foundations of the Polyhedral Model
- Extensions of the Polyhedral Model
- Tool Demonstration

------------------------------------------

Program Chairs and Organization

Christophe Alias, INRIA, France (chair)
Cédric Bastoul, U. of Paris-Sud, France (co-chair)

Program Committee

Muthu Baskaran, Reservoir Labs, Inc., USA
Uday Bondhugula, IISc Bangalore, India
Philippe Clauss, U. of Strasbourg, France
Albert Cohen, INRIA, France
Paul Feautrier, ENS Lyon, France
Armin Größlinger, U. of Passau, Germany
Ronan Keryell, HPC Project, France
Sebastian Pop, AMD, USA
Lakshminaraya Renganarayana, IBM, USA
J. Ramanujam, Louisiana State University, USA
S. Rajopadhye, Colorado State University, USA
P. Sadayappan, The Ohio State University, USA

------------------------------------------

Important dates

Submission deadline: February 9, 2011
Notification to authors: February 28, 2011
Final version due: March 18, 2011
Workshop: April 3, 2011, in conjunction with CGO 2011

------------------------------------------

Program

08:00 – Welcome
08:10 – Keynote: "Approximations in the Polyhedral Model"
Alain Darte, senior research scientist at CNRS
[abstract] [slides]

Session 1: Theory and Techniques
Chair: Uday Bondhugula

09:00 – Transparent Parallelization of Binary Code 
Benoît Pradelle, Alain Ketterlin and Philippe Clauss
[paper] [slides]

09:25 – Potential and Challenges of Two-Variable-Per-Inequality
Sub-Polyhedral Compilation 
Ramakrishna Upadrasta and Albert Cohen 
[paper] [slides]

09:50 – Pause

10:20 – More Definite Results from the PluTo Scheduling Algorithm 
Athanasios Konstantinidis, Paul H. J. Kelly
[paper] [slides]

Session 2: Tools
Chair: Vincent Loechner

10:45 – Counting Affine Calculator and Applications
Sven Verdoolaege
[paper] [slides] [demo]

11:10 – PIPS Is not (just) Polyhedral Software
Mehdi Amini, Corinne Ancourt, Fabien Coelho, Béatrice Creusillet,
Serge Guelton, François Irigoin, Pierre Jouvelot, Ronan Keryell,
Pierre Villalon
[paper] [slides]

11:35 – Polly – Polyhedral Optimization in LLVM
Tobias Grosser, Hongbin Zheng, Ragesh Aloor, Andreas Simbürger,
Armin Größlinger, Louis-Noël Pouchet
[paper] [slides]

12:00 – Closing


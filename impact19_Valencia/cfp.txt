This is a CALL FOR PAPERS for

    IMPACT 2019

9th International Workshop on 
Polyhedral Compilation Techniques

http://impact.gforge.inria.fr/impact2019/

held in conjunction with HiPEAC 2019 (Jan 21-23, 2019) in
Valencia, Spain.

======================================
IMPORTANT DATES:
Abstract submission: October  19, 2018 (AoE)
Paper submission:    October  26, 2018 (AoE)
Author notification: December  3, 2018
Final version due:   December 14, 2018
======================================

OVERVIEW:

With multicore processors and deep memory hierarchies remaining the main
source of performance, and emerging areas such as energy-aware
computation requiring research breakthroughs, polyhedral compilation
techniques receive more attention than ever from both researchers and
practitioners.  Thanks to a unified formalism for parallelization and
memory optimization, these techniques are now powering domain-specific
language compilers yielding best performance in highly competitive areas
such as machine learning and numeric simulation.  IMPACT is a unique
event focusing exclusively on polyhedral compilation that brings
together researchers and practitioners for a high-quality one-day event
including a keynote, selected paper presentations, and posters for
work-in-progress discussions.

We welcome both theoretical and experimental papers on all aspects of
polyhedral compilation and optimization. We also welcome submissions
describing preliminary results, crazy new ideas, position papers,
experience reports, and available tools, with an aim to stimulate
discussions, collaborations, and advances in the field. The following
illustrate potential IMPACT papers:

- Thorough theoretical discussion of a preliminary idea with an attempt to
  place it in context but no experimental results.

- Experimental results comparing two or more existing ideas, followed by a
  detailed analysis.

- Presentation of an existing idea in a different way, including
  illustrations of how the idea applies to new use cases, code,
  architectures, etc.  Attribution should as clear as possible.


Topics of interest include, but are not limited to:
- program optimization (automatic parallelization, tiling, etc.);
- code generation;
- data/communication management on GPUs, accelerators and distributed
  systems;
- hardware/high-level synthesis for affine programs;
- static analysis;
- program verification;
- model checking;
- theoretical foundations of the polyhedral model;
- extensions of the polyhedral model;
- scalability and robustness of polyhedral compilation techniques.


SUBMISSION:

Submissions should not exceed 10 pages (recommended 8 pages), excluding
references, formatted as per ACM SIGPLAN proceedings format.  Please use
version 1.54 or above of the following templates to prepare your
manuscript:

  https://www.acm.org/publications/proceedings-template

Make sure to use the "sigplan" subformat.  Visit

  http://sigplan.org/Resources/Author/

for further information on SIGPLAN manuscript formatting.
NOTE: older versions of the article template use smaller fonts for
submission, please double-check that you are using the recent style file
(in particular, various LaTeX distributions ship older versions of the
acmart style file, please download the most recent one from ACM).

Submissions should use PDF format and be printable on US Letter or A4
paper.  Please submit your manuscripts through EasyChair:

  https://easychair.org/conferences/?conf=impact2019

Proceedings will be posted online. If the final version of an accepted
paper does not sufficiently address the comments of the reviewers, then
it may be accompanied by a note from the program committee. Publication
at IMPACT will not prevent later publication in conferences or journals
of the presented work.  However, simultaneous submission to IMPACT and
other workshop, conference, or journal is often prohibited by the policy
of other venues. For instance, a manuscript overlapping significantly
with IMPACT submission cannot be submitted to PLDI 2019 or any other
overlapping SIGPLAN event.

We will also continue the poster teasers we started last year.  Authors
of the rejected papers that still plan to attend HiPEAC will have an
opportunity to present their submission in the HiPEAC poster session.
We encourage poster presentations by providing a short (3 min.) slot in
the workshop to advertise the posters.  If possible, posters related to
IMPACT will be gathered in a same vicinity at the poster session.

Please make sure that at least one of the authors can attend the
workshop if your work is accepted.

COMMITTEES:

Organizers and Program Chairs:

David Wonnacott  (Haverford College, USA)
Oleksandr Zinenko  (Inria / ENS Paris, France)
Contact: impact-chairs@lists.gforge.inria.fr

Program Committee:

Cedric Bastoul  (Univ. of Strasbourg, France)
Somashekaracharya Bhaskaracharya  (National Instruments, USA)
Albert Cohen  (Inria / ENS Paris, France)
Paul Feautrier  (ENS Paris, France)
Tobias Grosser  (ETH Zurich, Switzerland)
Mary Hall  (University of Utah, USA)
Guillaume Iooss  (ENS Paris / Inria, France)
Francois Irigoin  (Mines Paris Tech, France)
Paul Kelly  (Imperial College London, UK)
Andreas Kloeckner  (Univ. of Illinois Urbana Champaign USA)
Martin Kong  (Brookhaven National Lab, USA)
Michael Kruse  (Argonne National Lab, USA)
Karthik Murthy  (Stanford University, USA)
Louis-Noel Pouchet  (Colorado State Univ., USA)
Sanjay Rajopadhye  (Colorado State Univ., USA)
Jun Shirako  (GeorgiaTech, USA)
Ramakrishna Upadrasta  (IIT Hyderabad, India)
Nicolas Vasilache  (Google, USA)
Anand Venkat  (Intel, USA)
Sven Verdoolaege  (KU Leuven, Belgium)
Tomofumi Yuki  (Univ. of Rennes / IRISA, France)
 
